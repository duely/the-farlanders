package com.legacy.farlanders.world.structure;

import java.util.Random;

import com.legacy.farlanders.FarlandersConfig;
import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.registry.FarlandersEntityTypes;
import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.PaneBlock;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class FarlanderLootFeature extends Feature<NoFeatureConfig>
{
	private BlockState replaceID;

	// TODO: Replace this with a proper structure using Structure Gel
	public FarlanderLootFeature(Codec<NoFeatureConfig> configFactoryIn, BlockState replace)
	{
		super(configFactoryIn);
		this.replaceID = replace;
	}

	public boolean locationIsValidSpawn(IWorld world, int x, int y, int z)
	{
		int distanceToAir = 0;
		BlockState checkID = world.getBlockState(new BlockPos(x, y, z));

		for (; distanceToAir < 255 && checkID != Blocks.AIR.getDefaultState(); distanceToAir++)
		{
			checkID = world.getBlockState(new BlockPos(x, y + distanceToAir, z));

			if (distanceToAir > 100)
				return false;
			if (distanceToAir < 0)
				return false;
		}

		if (distanceToAir > 2)
		{
			return false;
		}

		y += distanceToAir - 1;
		BlockState blockID = world.getBlockState(new BlockPos(x, y, z));
		BlockState blockIDAbove = world.getBlockState(new BlockPos(x, y + 1, z));
		BlockState blockIDBelow = world.getBlockState(new BlockPos(x, y - 1, z));
		if (blockIDAbove != Blocks.AIR.getDefaultState())
		{
			return false;
		}
		if ((blockID.getBlock() == Blocks.SNOW && blockIDBelow == replaceID) || (blockID.getBlock() == Blocks.GRASS && blockIDBelow == replaceID) || (blockID == Blocks.POPPY.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.DANDELION.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.DEAD_BUSH.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.CACTUS.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.BROWN_MUSHROOM.getDefaultState() && blockIDBelow == replaceID) || (blockID == Blocks.RED_MUSHROOM.getDefaultState() && blockIDBelow == replaceID))
		{
			return true;
		}
		else if (blockID == replaceID)
		{
			return true;
		}
		return false;
	}

	public boolean airLocationIsValidSpawn(IWorld world, int x, int y, int z)
	{
		BlockState blockID = world.getBlockState(new BlockPos(x, y, z));
		BlockState blockIDAbove = world.getBlockState(new BlockPos(x, y + 1, z));
		if (blockID != Blocks.AIR.getDefaultState() || blockIDAbove != Blocks.AIR.getDefaultState())
		{
			return false;
		}
		return true;
	}

	@Override
	public boolean generate(ISeedReader worldIn, ChunkGenerator generatorIn, Random randomIn, BlockPos posIn, NoFeatureConfig config)
	{
		int i = posIn.getX();
		int j = posIn.getY();
		int z = posIn.getZ();

		int doorSide = randomIn.nextInt(2);
		BlockState endStone = Blocks.END_STONE.getDefaultState();
		BlockState ironBars = Blocks.IRON_BARS.getDefaultState();
		// TileEntityChest chest = new TileEntityChest();

		if (!locationIsValidSpawn(worldIn, i - 1, j, z - 2) || !locationIsValidSpawn(worldIn, i + 3, j, z - 2) || !locationIsValidSpawn(worldIn, i + 3, j, z + 2) || !locationIsValidSpawn(worldIn, i - 1, j, z + 2) || !airLocationIsValidSpawn(worldIn, i - 1, j + 3, z - 2) || !airLocationIsValidSpawn(worldIn, i + 3, j + 3, z - 2) || !airLocationIsValidSpawn(worldIn, i + 3, j + 3, z + 2) || !airLocationIsValidSpawn(worldIn, i - 1, j + 3, z + 2))
		{
			return false;
		}

		if (FarlandersConfig.COMMON.farlanderLootWellSpawnRate.get() > 1 && randomIn.nextInt(FarlandersConfig.COMMON.farlanderLootWellSpawnRate.get()) != 0 || FarlandersConfig.COMMON.farlanderLootWellSpawnRate.get() <= 1)
		{
			return false;
		}

		/*System.out.println(new BlockPos(i, j, k));*/

		worldIn.setBlockState(new BlockPos(i, j, z + 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i, j, z - 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i, j + 1, z + 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i, j + 1, z - 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i, j + 2, z), endStone, 2);
		worldIn.setBlockState(new BlockPos(i, j + 2, z + 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i, j + 2, z - 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 1, j, z + 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 1, j, z - 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 1, j + 1, z), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i + 1, j + 1, z + 1), ironBars.with(PaneBlock.EAST, true).with(PaneBlock.WEST, true), 2);
		worldIn.setBlockState(new BlockPos(i + 1, j + 1, z - 1), ironBars.with(PaneBlock.EAST, true).with(PaneBlock.WEST, true), 2);
		worldIn.setBlockState(new BlockPos(i + 1, j + 2, z), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 1, j + 2, z - 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 1, j + 2, z + 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 1, j + 3, z), Blocks.SMOOTH_STONE_SLAB.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i + 2, j, z + 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 2, j, z - 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 2, j + 1, z + 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 2, j + 1, z - 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 2, j + 2, z), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 2, j + 2, z + 1), endStone, 2);
		worldIn.setBlockState(new BlockPos(i + 2, j + 2, z - 1), endStone, 2);

		if (doorSide != 0)
		{
			worldIn.setBlockState(new BlockPos(i + 2, j, z), endStone, 2);
			worldIn.setBlockState(new BlockPos(i + 2, j + 1, z), endStone, 2);
		}
		else
		{
			worldIn.setBlockState(new BlockPos(i + 2, j, z), ironBars.with(PaneBlock.NORTH, true).with(PaneBlock.SOUTH, true), 2);
			worldIn.setBlockState(new BlockPos(i + 2, j + 1, z), ironBars.with(PaneBlock.NORTH, true).with(PaneBlock.SOUTH, true), 2);
		}
		if (doorSide != 1)
		{
			worldIn.setBlockState(new BlockPos(i, j, z), endStone, 2);
			worldIn.setBlockState(new BlockPos(i, j + 1, z), endStone, 2);
		}
		else
		{
			worldIn.setBlockState(new BlockPos(i, j, z), ironBars.with(PaneBlock.NORTH, true).with(PaneBlock.SOUTH, true), 2);
			worldIn.setBlockState(new BlockPos(i, j + 1, z), ironBars.with(PaneBlock.NORTH, true).with(PaneBlock.SOUTH, true), 2);
		}

		worldIn.setBlockState(new BlockPos(i + 1, j, z), Blocks.CHEST.getDefaultState(), 2); // Small Loot Chest
		ChestTileEntity tile = (ChestTileEntity) worldIn.getTileEntity(new BlockPos(i + 1, j, z));
		tile.setLootTable(TheFarlandersMod.locate("chests/small_loot_chest"), worldIn.getRandom().nextLong());

		worldIn.setBlockState(new BlockPos(i, j, z - 2), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i, j + 1, z - 2), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i, j + 2, z - 2), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i + 3, j, z), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i + 3, j + 1, z), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i + 3, j + 2, z), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i - 1, j, z), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i - 1, j + 1, z), Blocks.AIR.getDefaultState(), 2);
		worldIn.setBlockState(new BlockPos(i - 1, j + 2, z), Blocks.AIR.getDefaultState(), 2);

		/*for (int posX = i; posX <= i + 8; posX++)
			for (int posZ = z; posZ <= z + 6; posZ++)
			{
				int posY = j - 1;
				boolean isFloorFinished = false;
				while (isFloorFinished == false)
				{
					if (worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.AIR.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.GRASS.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.CACTUS.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.POPPY.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.DANDELION.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.DEAD_BUSH.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.SNOW.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.WATER.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.LAVA.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.BROWN_MUSHROOM.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.RED_MUSHROOM.getDefaultState() || worldIn.getBlockState(new BlockPos(posX, posY, posZ)) == Blocks.LILY_PAD.getDefaultState())
					{
						worldIn.setBlockState(new BlockPos(posX, posY, posZ), endStone, 2);
						posY--;
					}
					else
						isFloorFinished = true;
				}
			}*/

		TitanEntity titan = new TitanEntity(FarlandersEntityTypes.TITAN, worldIn.getWorld());
		titan.setPosition(i - 1, j + 1, z);
		titan.enablePersistence();
		worldIn.addEntity(titan);

		EnderGolemEntity golem = new EnderGolemEntity(FarlandersEntityTypes.ENDER_GOLEM, worldIn.getWorld());
		golem.setPosition(i - 1, j + 1, z);
		golem.enablePersistence();
		worldIn.addEntity(golem);

		return true;
	}
}