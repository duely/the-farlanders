package com.legacy.farlanders.world.structure;

import java.util.Random;

import com.google.common.collect.Lists;
import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.legacy.farlanders.registry.FarlandersEntityTypes;
import com.legacy.farlanders.registry.FarlandersStructures;
import com.legacy.structure_gel.util.ConfigTemplates.StructureConfig;
import com.legacy.structure_gel.worldgen.jigsaw.AbstractGelStructurePiece;
import com.legacy.structure_gel.worldgen.jigsaw.GelConfigJigsawStructure;
import com.mojang.serialization.Codec;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.SpawnReason;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.util.Rotation;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.jigsaw.JigsawManager.IPieceFactory;
import net.minecraft.world.gen.feature.jigsaw.JigsawPiece;
import net.minecraft.world.gen.feature.structure.IStructurePieceType;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.VillageConfig;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.minecraft.world.gen.settings.StructureSeparationSettings;

public class FarlanderVillageStructure extends GelConfigJigsawStructure
{
	public FarlanderVillageStructure(Codec<VillageConfig> configFactoryIn, StructureConfig config)
	{
		super(configFactoryIn, config, 0, true, true);
		this.setSpawnList(EntityClassification.MONSTER, Lists.newArrayList());
	}

	@Override
	protected boolean func_230363_a_(ChunkGenerator chunkGen, BiomeProvider biomeProvider, long seed, SharedSeedRandom sharedSeedRand, int chunkPosX, int chunkPosZ, Biome biomeIn, ChunkPos chunkPos, VillageConfig config)
	{
		return super.func_230363_a_(chunkGen, biomeProvider, seed, sharedSeedRand, chunkPosX, chunkPosZ, biomeIn, chunkPos, config) && !isVillageClose(chunkGen, seed, sharedSeedRand, chunkPosX, chunkPosZ);
	}

	private boolean isVillageClose(ChunkGenerator chunkGen, long seed, SharedSeedRandom sharedSeedRand, int chunkPosX, int chunkPosZ)
	{
		StructureSeparationSettings structureseparationsettings = chunkGen.func_235957_b_().func_236197_a_(Structure.VILLAGE);
		if (structureseparationsettings == null)
		{
			return false;
		}
		else
		{
			int range = 5;
			for (int x = chunkPosX - range; x <= chunkPosX + range; ++x)
			{
				for (int z = chunkPosZ - range; z <= chunkPosZ + range; ++z)
				{
					ChunkPos chunkpos = Structure.VILLAGE.getChunkPosForStructure(structureseparationsettings, seed, sharedSeedRand, x, z);
					if (x == chunkpos.x && z == chunkpos.z)
						return true;
				}
			}
		}
		return false;
	}

	@Override
	public int getSeed()
	{
		return 997638;
	}

	@Override
	public IPieceFactory getPieceType()
	{
		return FarlanderVillageStructure.Piece::new;
	}

	public static class Piece extends AbstractGelStructurePiece
	{
		public Piece(TemplateManager template, JigsawPiece jigsawPiece, BlockPos pos, int groundLevelDelta, Rotation rotation, MutableBoundingBox boundingBox)
		{
			super(template, jigsawPiece, pos, groundLevelDelta, rotation, boundingBox);
		}

		public Piece(TemplateManager template, CompoundNBT nbt)
		{
			super(template, nbt);
		}

		@Override
		public IStructurePieceType getStructurePieceType()
		{
			return FarlandersStructures.FARLANDER_VILLAGE.getPieceType();
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, IServerWorld worldIn, Random rand, MutableBoundingBox bounds)
		{
			if (key.equals("farlander"))
			{
				setAir(worldIn, pos);

				FarlanderEntity entity = createEntity(FarlandersEntityTypes.FARLANDER, worldIn, pos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.equals("elder"))
			{
				setAir(worldIn, pos);
				ElderFarlanderEntity entity = createEntity(FarlandersEntityTypes.ELDER_FARLANDER, worldIn, pos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.equals("golem"))
			{
				setAir(worldIn, pos);

				EnderGolemEntity entity = createEntity(FarlandersEntityTypes.ENDER_GOLEM, worldIn, pos, this.rotation);
				entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
				entity.enablePersistence();
				worldIn.addEntity(entity);
			}

			if (key.contains("guardian"))
			{
				if (key.contains("up"))
				{
					worldIn.setBlockState(pos, Blocks.GRASS_PATH.getDefaultState(), 3);

					EnderGuardianEntity entity = createEntity(FarlandersEntityTypes.ENDER_GUARDIAN, worldIn, pos.up(), this.rotation);
					entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos.up()), SpawnReason.STRUCTURE, null, null);
					entity.enablePersistence();
					worldIn.addEntity(entity);
				}
				else
				{
					setAir(worldIn, pos);

					EnderGuardianEntity entity = createEntity(FarlandersEntityTypes.ENDER_GUARDIAN, worldIn, pos, this.rotation);
					entity.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
					entity.enablePersistence();
					worldIn.addEntity(entity);
				}
			}

			if (key.equals("elder-loot"))
			{
				setAir(worldIn, pos);

				ChestTileEntity tile = (ChestTileEntity) worldIn.getTileEntity(pos.down());
				tile.setLootTable(TheFarlandersMod.locate("chests/elder_house_chest"), worldIn.getRandom().nextLong());
			}
		}
	}
}
