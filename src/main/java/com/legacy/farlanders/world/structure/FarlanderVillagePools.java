package com.legacy.farlanders.world.structure;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FarlandersProcessors;
import com.legacy.structure_gel.worldgen.jigsaw.JigsawRegistryHelper;

import net.minecraft.world.gen.feature.jigsaw.JigsawPattern;
import net.minecraft.world.gen.feature.jigsaw.JigsawPattern.PlacementBehaviour;

public class FarlanderVillagePools
{
	public static final JigsawPattern ROOT;

	public static void init()
	{
	}

	static
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(TheFarlandersMod.MODID, "village/normal/");
		ROOT = registry.register("centers", registry.builder().names("centers/watch_tower", "centers/wooden_tower", "centers/gazebo").maintainWater(false).processors(FarlandersProcessors.PATH_TO_DIRT_TO_PLANKS).build());
		registry.register("streets", registry.builder().names("streets/path_1", "streets/path_2", "streets/path_3", "streets/path_4").maintainWater(false).processors(FarlandersProcessors.PATH_TO_DIRT_TO_PLANKS).build(), PlacementBehaviour.TERRAIN_MATCHING);
		registry.register("house", registry.builder().names("house_1", "house_2", "house_3", "house_4").maintainWater(false).build());
		registry.register("house_special", registry.builder().names("house_special_1", "house_special_2").maintainWater(false).processors(FarlandersProcessors.LEAVES_TO_GROUND).build());
	}
}
