package com.legacy.farlanders.event;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.legacy.farlanders.registry.FarlandersItems;

import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FarlandersEvents
{
	private static final List<Item> NIGHTFALL_ARMOR = ImmutableList.of(FarlandersItems.nightfall_helmet, FarlandersItems.nightfall_chestplate, FarlandersItems.nightfall_leggings, FarlandersItems.nightfall_boots);

	@SubscribeEvent
	public void onLivingTick(LivingUpdateEvent event)
	{
		if (!event.getEntityLiving().world.isDaytime() && event.getEntityLiving().ticksExisted % 20 == 0)
		{
			if (isWearingNightfallOnSlot(event.getEntityLiving(), EquipmentSlotType.HEAD))
			{
				event.getEntityLiving().addPotionEffect(new EffectInstance(Effects.NIGHT_VISION, 12 * 20, 0, true, true));
			}

			if (isWearingNightfallOnSlot(event.getEntityLiving(), EquipmentSlotType.CHEST))
			{
				event.getEntityLiving().addPotionEffect(new EffectInstance(Effects.RESISTANCE, 12 * 20, 0, true, true));
			}

			if (isWearingNightfallOnSlot(event.getEntityLiving(), EquipmentSlotType.LEGS))
			{
				event.getEntityLiving().addPotionEffect(new EffectInstance(Effects.SPEED, 12 * 20, 0, true, true));
			}

			if (isWearingNightfallOnSlot(event.getEntityLiving(), EquipmentSlotType.FEET))
			{
				event.getEntityLiving().addPotionEffect(new EffectInstance(Effects.JUMP_BOOST, 12 * 20, 0, true, true));
			}
		}
	}

	public static boolean isWearingNightfallOnSlot(LivingEntity living, EquipmentSlotType pieceValue)
	{
		return NIGHTFALL_ARMOR.contains(living.getItemStackFromSlot(pieceValue).getItem());
	}
}
