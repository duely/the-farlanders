package com.legacy.farlanders.registry;

import com.google.common.collect.ImmutableList;
import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.structure_gel.util.RegistryHelper;

import net.minecraft.block.Blocks;
import net.minecraft.world.gen.feature.template.AlwaysTrueRuleTest;
import net.minecraft.world.gen.feature.template.BlockMatchRuleTest;
import net.minecraft.world.gen.feature.template.RandomBlockMatchRuleTest;
import net.minecraft.world.gen.feature.template.RuleEntry;
import net.minecraft.world.gen.feature.template.RuleStructureProcessor;
import net.minecraft.world.gen.feature.template.StructureProcessor;
import net.minecraft.world.gen.feature.template.StructureProcessorList;

public class FarlandersProcessors
{
	public static final StructureProcessorList STAIRS_TO_GRASS = register("stairs_to_grass", new RuleStructureProcessor(ImmutableList.of(new RuleEntry(new BlockMatchRuleTest(Blocks.STONE_BRICK_STAIRS), new BlockMatchRuleTest(Blocks.GRASS_BLOCK), Blocks.GRASS_BLOCK.getDefaultState()), new RuleEntry(new BlockMatchRuleTest(Blocks.STONE_BRICK_STAIRS), new BlockMatchRuleTest(Blocks.DIRT), Blocks.DIRT.getDefaultState()), new RuleEntry(new BlockMatchRuleTest(Blocks.STONE_BRICK_STAIRS), new BlockMatchRuleTest(Blocks.STONE), Blocks.STONE.getDefaultState()))));
	public static final StructureProcessorList LEAVES_TO_GROUND = register("leaves_to_ground", new RuleStructureProcessor(ImmutableList.of(new RuleEntry(new BlockMatchRuleTest(Blocks.OAK_LEAVES), new BlockMatchRuleTest(Blocks.GRASS_BLOCK), Blocks.GRASS_BLOCK.getDefaultState()), new RuleEntry(new BlockMatchRuleTest(Blocks.OAK_LEAVES), new BlockMatchRuleTest(Blocks.DIRT), Blocks.DIRT.getDefaultState()), new RuleEntry(new BlockMatchRuleTest(Blocks.OAK_LEAVES), new BlockMatchRuleTest(Blocks.STONE), Blocks.STONE.getDefaultState()))));
	public static final StructureProcessorList PATH_TO_DIRT_TO_PLANKS = register("path_to_dirt_to_planks", new RuleStructureProcessor(ImmutableList.of(new RuleEntry(new BlockMatchRuleTest(Blocks.GRASS_PATH), new BlockMatchRuleTest(Blocks.WATER), Blocks.OAK_PLANKS.getDefaultState()), new RuleEntry(new RandomBlockMatchRuleTest(Blocks.GRASS_PATH, 0.1F), AlwaysTrueRuleTest.INSTANCE, Blocks.GRASS_BLOCK.getDefaultState()), new RuleEntry(new BlockMatchRuleTest(Blocks.GRASS_BLOCK), new BlockMatchRuleTest(Blocks.WATER), Blocks.WATER.getDefaultState()), new RuleEntry(new BlockMatchRuleTest(Blocks.DIRT), new BlockMatchRuleTest(Blocks.WATER), Blocks.WATER.getDefaultState()))));

	private static StructureProcessorList register(String key, StructureProcessor processor)
	{
		return RegistryHelper.registerProcessor(TheFarlandersMod.locate(key), processor);
	}
}
