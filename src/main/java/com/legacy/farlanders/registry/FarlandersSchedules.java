package com.legacy.farlanders.registry;

import com.legacy.farlanders.FarlandersRegistry;

import net.minecraft.entity.ai.brain.schedule.Activity;
import net.minecraft.entity.ai.brain.schedule.Schedule;
import net.minecraft.entity.ai.brain.schedule.ScheduleBuilder;
import net.minecraftforge.event.RegistryEvent.Register;

public class FarlandersSchedules
{
	public static final Schedule FARLANDER_DEFAULT = buildSchedule("farlander_default").add(0, Activity.REST).add(13000, Activity.IDLE).add(19000, Activity.MEET).add(20000, Activity.IDLE).build();
	public static final Schedule FARLANDER_BABY = buildSchedule("farlander_baby").add(0, Activity.REST).add(13000, Activity.IDLE).add(16000, Activity.PLAY).add(18000, Activity.IDLE).add(20000, Activity.PLAY).build();

	private static ScheduleBuilder buildSchedule(String string)
	{
		return new ScheduleBuilder(new Schedule());
	}

	public static void init(Register<Schedule> event)
	{
		FarlandersRegistry.register(event.getRegistry(), "farlander_default", FARLANDER_DEFAULT);
		FarlandersRegistry.register(event.getRegistry(), "farlander_baby", FARLANDER_BABY);
	}
}
