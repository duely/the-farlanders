package com.legacy.farlanders.registry;

import com.legacy.farlanders.FarlandersRegistry;
import com.legacy.farlanders.world.structure.FarlanderLootFeature;
import com.legacy.farlanders.world.structure.TitanSpireFeature;
import com.legacy.structure_gel.access_helpers.BiomeAccessHelper;
import com.legacy.structure_gel.biome_dictionary.BiomeDictionary;
import com.legacy.structure_gel.biome_dictionary.BiomeType;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityClassification;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.MobSpawnInfo;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.Features;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.ReplaceBlockConfig;
import net.minecraft.world.gen.placement.ChanceConfig;
import net.minecraft.world.gen.placement.IPlacementConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.common.world.MobSpawnInfoBuilder;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FarlandersFeatures
{
	public static final Feature<NoFeatureConfig> LOOT_WELL = new FarlanderLootFeature(NoFeatureConfig.field_236558_a_, Blocks.GRASS_BLOCK.getDefaultState());
	public static final Feature<NoFeatureConfig> TITAN_SPIRE = new TitanSpireFeature(NoFeatureConfig.field_236558_a_);

	@SubscribeEvent
	public static void init(RegistryEvent.Register<Feature<?>> event)
	{
		FarlandersRegistry.register(event.getRegistry(), "loot_well", LOOT_WELL);
		FarlandersRegistry.register(event.getRegistry(), "titan_spire", TITAN_SPIRE);
	}

	public static void addFeatures(BiomeLoadingEvent event)
	{
		if (event.getCategory() == Biome.Category.MUSHROOM)
			return;

		BiomeGenerationSettingsBuilder builder = event.getGeneration();
		MobSpawnInfoBuilder spawns = event.getSpawns();

		if (event.getCategory() != Biome.Category.THEEND && event.getCategory() != Biome.Category.NETHER && event.getCategory() != Biome.Category.NONE && event.getCategory() != Biome.Category.RIVER && event.getCategory() != Biome.Category.OCEAN && !containsBiome(BiomeDictionary.NETHER, event) && !containsBiome(BiomeDictionary.END, event) && !containsBiome(BiomeDictionary.COLD, event) && !containsBiome(BiomeDictionary.RIVER, event) && !containsBiome(BiomeDictionary.OCEAN, event))
		{
			if (containsBiome(BiomeDictionary.PLAINS, event) && !containsBiome(BiomeDictionary.COLD, event) && !containsBiome(BiomeDictionary.SNOWY, event) && !containsBiome(BiomeDictionary.HOT, event) && event.getCategory() != Biome.Category.FOREST)
				BiomeAccessHelper.addStructure(event, FarlandersStructures.FARLANDER_VILLAGE.getStructureFeature());

			BiomeAccessHelper.addStructure(event, FarlandersStructures.SMALL_HOUSE.getStructureFeature());

			builder.withFeature(GenerationStage.Decoration.RAW_GENERATION, Configured.LOOT_WELL);
			builder.withFeature(GenerationStage.Decoration.RAW_GENERATION, Configured.TITAN_SPIRE);

			spawns.withSpawner(EntityClassification.CREATURE, new MobSpawnInfo.Spawners(FarlandersEntityTypes.ENDERMINION, 2, 1, 1));
			spawns.withSpawner(EntityClassification.CREATURE, new MobSpawnInfo.Spawners(FarlandersEntityTypes.MYSTIC_ENDERMINION, 1, 1, 1));
			spawns.withSpawner(EntityClassification.CREATURE, new MobSpawnInfo.Spawners(FarlandersEntityTypes.WANDERER, 3, 1, 1));

			spawns.withSpawner(EntityClassification.MONSTER, new MobSpawnInfo.Spawners(FarlandersEntityTypes.LOOTER, 20, 1, 2));
			spawns.withSpawner(EntityClassification.MONSTER, new MobSpawnInfo.Spawners(FarlandersEntityTypes.REBEL, 20, 1, 4));
			spawns.withSpawner(EntityClassification.MONSTER, new MobSpawnInfo.Spawners(FarlandersEntityTypes.CLASSIC_ENDERMAN, 7, 1, 4));
			spawns.withSpawner(EntityClassification.MONSTER, new MobSpawnInfo.Spawners(FarlandersEntityTypes.FANMADE_ENDERMAN, 5, 1, 2));
			spawns.withSpawner(EntityClassification.MONSTER, new MobSpawnInfo.Spawners(FarlandersEntityTypes.MYSTIC_ENDERMAN, 3, 1, 2));
		}

		if (BiomeDictionary.MOUNTAIN.getBiomes().contains(event.getName()))
			builder.withFeature(GenerationStage.Decoration.RAW_GENERATION, Configured.ENDUMIUM_ORE);
	}

	public static boolean doesBiomeMatch(Biome biomeIn, RegistryKey<Biome> wantedBiomeIn)
	{
		return biomeIn.toString().matches(wantedBiomeIn.getLocation().toString());
	}

	public static boolean containsBiome(BiomeType biomeIn, BiomeLoadingEvent eventIn)
	{
		return biomeIn.getBiomes().contains(eventIn.getName());
	}

	public static class Configured
	{
		public static final ConfiguredFeature<?, ?> LOOT_WELL = FarlandersFeatures.LOOT_WELL.withConfiguration(IFeatureConfig.NO_FEATURE_CONFIG).withPlacement(Features.Placements.PATCH_PLACEMENT).func_242731_b(1);
		public static final ConfiguredFeature<?, ?> TITAN_SPIRE = FarlandersFeatures.TITAN_SPIRE.withConfiguration(IFeatureConfig.NO_FEATURE_CONFIG).withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT).withPlacement(Placement.CHANCE.configure(new ChanceConfig(1)));
		public static final ConfiguredFeature<?, ?> ENDUMIUM_ORE = Feature.EMERALD_ORE.withConfiguration(new ReplaceBlockConfig(Blocks.STONE.getDefaultState(), FarlandersBlocks.endumium_ore.getDefaultState())).withPlacement(Placement.EMERALD_ORE.configure(IPlacementConfig.NO_PLACEMENT_CONFIG));

		public static void init()
		{
			register("loot_well", LOOT_WELL);
			register("titan_spire", TITAN_SPIRE);
			register("endumium_ore", ENDUMIUM_ORE);
		}

		private static <FC extends IFeatureConfig> ConfiguredFeature<FC, ?> register(String nameIn, ConfiguredFeature<FC, ?> featureIn)
		{
			return Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, nameIn, featureIn);
		}
	}
}
