package com.legacy.farlanders.registry;

import java.util.Random;

import com.legacy.farlanders.FarlandersRegistry;
import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.hostile.ClassicEndermanEntity;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.legacy.farlanders.entity.hostile.FanmadeEndermanEntity;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.legacy.farlanders.entity.hostile.MysticEndermanEntity;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.entity.hostile.boss.EnderColossusEntity;
import com.legacy.farlanders.entity.hostile.boss.summon.EnderColossusShadowEntity;
import com.legacy.farlanders.entity.hostile.boss.summon.MiniDragonEntity;
import com.legacy.farlanders.entity.hostile.boss.summon.ThrownBlockEntity;
import com.legacy.farlanders.entity.hostile.nightfall.EndSpiritEntity;
import com.legacy.farlanders.entity.hostile.nightfall.NightfallSpiritEntity;
import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.legacy.farlanders.entity.tameable.MysticEnderminionEntity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(TheFarlandersMod.MODID)
public class FarlandersEntityTypes
{
	public static final EntityType<FarlanderEntity> FARLANDER = buildEntity("farlander", EntityType.Builder.create(FarlanderEntity::new, EntityClassification.CREATURE).size(0.6F, 1.8F));
	public static final EntityType<ElderFarlanderEntity> ELDER_FARLANDER = buildEntity("elder_farlander", EntityType.Builder.create(ElderFarlanderEntity::new, EntityClassification.CREATURE).size(0.6F, 1.8F));
	public static final EntityType<WandererEntity> WANDERER = buildEntity("wanderer", EntityType.Builder.create(WandererEntity::new, EntityClassification.CREATURE).size(0.6F, 1.8F));
	public static final EntityType<EnderGuardianEntity> ENDER_GUARDIAN = buildEntity("ender_guardian", EntityType.Builder.create(EnderGuardianEntity::new, EntityClassification.CREATURE).size(0.6F, 1.8F));
	public static final EntityType<LooterEntity> LOOTER = buildEntity("looter", EntityType.Builder.create(LooterEntity::new, EntityClassification.MONSTER).size(0.6F, 1.8F));
	public static final EntityType<RebelEntity> REBEL = buildEntity("rebel", EntityType.Builder.create(RebelEntity::new, EntityClassification.MONSTER).size(0.6F, 1.8F));
	public static final EntityType<EnderminionEntity> ENDERMINION = buildEntity("enderminion", EntityType.Builder.create(EnderminionEntity::new, EntityClassification.CREATURE).size(0.8F, 2.35F));
	public static final EntityType<MysticEnderminionEntity> MYSTIC_ENDERMINION = buildEntity("mystic_enderminion", EntityType.Builder.create(MysticEnderminionEntity::new, EntityClassification.CREATURE).size(0.8F, 2.35F));
	public static final EntityType<MysticEndermanEntity> MYSTIC_ENDERMAN = buildEntity("mystic_enderman", EntityType.Builder.create(MysticEndermanEntity::new, EntityClassification.MONSTER).size(0.6F, 2.9F));
	public static final EntityType<ClassicEndermanEntity> CLASSIC_ENDERMAN = buildEntity("classic_enderman", EntityType.Builder.create(ClassicEndermanEntity::new, EntityClassification.MONSTER).size(0.6F, 2.9F));
	public static final EntityType<FanmadeEndermanEntity> FANMADE_ENDERMAN = buildEntity("fanmade_enderman", EntityType.Builder.create(FanmadeEndermanEntity::new, EntityClassification.MONSTER).size(0.6F, 2.9F));
	public static final EntityType<EnderGolemEntity> ENDER_GOLEM = buildEntity("ender_golem", EntityType.Builder.create(EnderGolemEntity::new, EntityClassification.MONSTER).size(1.2F, 4.0F));
	public static final EntityType<TitanEntity> TITAN = buildEntity("titan", EntityType.Builder.create(TitanEntity::new, EntityClassification.MONSTER).size(1.8F, 5.3F));

	public static final EntityType<EndSpiritEntity> END_SPIRIT = buildEntity("end_spirit", EntityType.Builder.create(EndSpiritEntity::new, EntityClassification.CREATURE).size(0.6F, 1.9F));
	public static final EntityType<NightfallSpiritEntity> NIGHTFALL_SPIRIT = buildEntity("nightfall_spirit", EntityType.Builder.create(NightfallSpiritEntity::new, EntityClassification.CREATURE).size(0.6F, 1.9F));

	public static final EntityType<EnderColossusEntity> ENDER_COLOSSUS = buildEntity("ender_colossus", EntityType.Builder.create(EnderColossusEntity::new, EntityClassification.MONSTER).immuneToFire().size(3.6F, 21.0F));
	public static final EntityType<MiniDragonEntity> MINI_ENDER_DRAGON = buildEntity("mini_ender_dragon", EntityType.Builder.create(MiniDragonEntity::new, EntityClassification.MONSTER).immuneToFire().size(4.0F, 2.5F));
	public static final EntityType<EnderColossusShadowEntity> ENDER_COLOSSUS_SHADOW = buildEntity("ender_colossus_shadow", EntityType.Builder.create(EnderColossusShadowEntity::new, EntityClassification.MONSTER).immuneToFire().size(3.5F, 15.5F));
	public static final EntityType<ThrownBlockEntity> THROWN_BLOCK = buildEntity("thrown_block", EntityType.Builder.<ThrownBlockEntity>create(ThrownBlockEntity::new, EntityClassification.MISC).setCustomClientFactory(ThrownBlockEntity::new).setShouldReceiveVelocityUpdates(true).immuneToFire().size(3.0F, 3.0F));

	public static void init(Register<EntityType<?>> event)
	{
		FarlandersRegistry.register(event.getRegistry(), "farlander", FARLANDER);
		FarlandersRegistry.register(event.getRegistry(), "elder_farlander", ELDER_FARLANDER);
		FarlandersRegistry.register(event.getRegistry(), "wanderer", WANDERER);
		FarlandersRegistry.register(event.getRegistry(), "ender_guardian", ENDER_GUARDIAN);
		FarlandersRegistry.register(event.getRegistry(), "looter", LOOTER);
		FarlandersRegistry.register(event.getRegistry(), "rebel", REBEL);
		FarlandersRegistry.register(event.getRegistry(), "enderminion", ENDERMINION);
		FarlandersRegistry.register(event.getRegistry(), "mystic_enderminion", MYSTIC_ENDERMINION);
		FarlandersRegistry.register(event.getRegistry(), "mystic_enderman", MYSTIC_ENDERMAN);
		FarlandersRegistry.register(event.getRegistry(), "classic_enderman", CLASSIC_ENDERMAN);
		FarlandersRegistry.register(event.getRegistry(), "fanmade_enderman", FANMADE_ENDERMAN);
		FarlandersRegistry.register(event.getRegistry(), "ender_golem", ENDER_GOLEM);
		FarlandersRegistry.register(event.getRegistry(), "titan", TITAN);

		/*FarlandersRegistry.register(event.getRegistry(), "end_spirit", END_SPIRIT);
		FarlandersRegistry.register(event.getRegistry(), "nightfall_spirit", NIGHTFALL_SPIRIT);
		
		FarlandersRegistry.register(event.getRegistry(), "ender_colossus", ENDER_COLOSSUS);
		FarlandersRegistry.register(event.getRegistry(), "mini_ender_dragon", MINI_ENDER_DRAGON);
		FarlandersRegistry.register(event.getRegistry(), "ender_colossus_shadow", ENDER_COLOSSUS_SHADOW);
		FarlandersRegistry.register(event.getRegistry(), "thrown_block", THROWN_BLOCK);*/
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(TheFarlandersMod.find(key));
	}

	public static boolean minionCanSpawnOn(EntityType<? extends MobEntity> typeIn, IWorld worldIn, SpawnReason reason, BlockPos pos, Random randomIn)
	{
		return reason != SpawnReason.SPAWNER && randomIn.nextBoolean() && MobEntity.canSpawnOn(typeIn, worldIn, reason, pos, randomIn) || reason == SpawnReason.SPAWNER && MobEntity.canSpawnOn(typeIn, worldIn, reason, pos, randomIn);
	}

	public static void registerSpawnPlacements()
	{
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.FARLANDER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MobEntity::canSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.ELDER_FARLANDER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MobEntity::canSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.WANDERER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MobEntity::canSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.ENDER_GUARDIAN, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MobEntity::canSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.LOOTER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::canMonsterSpawnInLight);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.REBEL, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::canMonsterSpawnInLight);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.ENDERMINION, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, FarlandersEntityTypes::minionCanSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.MYSTIC_ENDERMINION, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, FarlandersEntityTypes::minionCanSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.MYSTIC_ENDERMAN, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::canMonsterSpawnInLight);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.CLASSIC_ENDERMAN, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::canMonsterSpawnInLight);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.FANMADE_ENDERMAN, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::canMonsterSpawnInLight);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.ENDER_GOLEM, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MobEntity::canSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.TITAN, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MobEntity::canSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.END_SPIRIT, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MobEntity::canSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.NIGHTFALL_SPIRIT, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MobEntity::canSpawnOn);
		EntitySpawnPlacementRegistry.register(FarlandersEntityTypes.ENDER_COLOSSUS, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::canMonsterSpawnInLight);

		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.FARLANDER, FarlanderEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.ELDER_FARLANDER, ElderFarlanderEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.WANDERER, WandererEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.ENDER_GUARDIAN, EnderGuardianEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.LOOTER, LooterEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.REBEL, RebelEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.ENDERMINION, EnderminionEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.MYSTIC_ENDERMINION, MysticEnderminionEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.MYSTIC_ENDERMAN, MysticEndermanEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.CLASSIC_ENDERMAN, ClassicEndermanEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.FANMADE_ENDERMAN, FanmadeEndermanEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.ENDER_GOLEM, EnderGolemEntity.registerAttributeMap().create());
		GlobalEntityTypeAttributes.put(FarlandersEntityTypes.TITAN, TitanEntity.registerAttributeMap().create());
	}
}
