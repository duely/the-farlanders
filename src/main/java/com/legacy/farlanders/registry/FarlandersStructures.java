package com.legacy.farlanders.registry;

import com.legacy.farlanders.FarlandersConfig;
import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.world.structure.FarlanderVillagePools;
import com.legacy.farlanders.world.structure.FarlanderVillageStructure;
import com.legacy.farlanders.world.structure.SmallHousePools;
import com.legacy.farlanders.world.structure.SmallHouseStructure;
import com.legacy.structure_gel.access_helpers.JigsawAccessHelper;
import com.legacy.structure_gel.registrars.StructureRegistrar;
import com.legacy.structure_gel.util.RegistryHelper;

import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.VillageConfig;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FarlandersStructures
{
	public static StructureRegistrar<VillageConfig, SmallHouseStructure> SMALL_HOUSE = RegistryHelper.handleRegistrar(StructureRegistrar.of(TheFarlandersMod.locate("small_house"), new SmallHouseStructure(VillageConfig.field_236533_a_, FarlandersConfig.COMMON.smallHouseStructureConfig), SmallHouseStructure.Piece::new, new VillageConfig(() -> SmallHousePools.ROOT, 7), Decoration.SURFACE_STRUCTURES));
	public static StructureRegistrar<VillageConfig, FarlanderVillageStructure> FARLANDER_VILLAGE = RegistryHelper.handleRegistrar(StructureRegistrar.of(TheFarlandersMod.locate("farlander_village"), new FarlanderVillageStructure(VillageConfig.field_236533_a_, FarlandersConfig.COMMON.farlanderVillageStructureConfig), FarlanderVillageStructure.Piece::new, new VillageConfig(() -> FarlanderVillagePools.ROOT, 7), Decoration.SURFACE_STRUCTURES));

	@SubscribeEvent
	public static void init(RegistryEvent.Register<Structure<?>> event)
	{
		SmallHousePools.init();
		RegistryHelper.handleRegistrar(SMALL_HOUSE, event.getRegistry());

		FarlanderVillagePools.init();
		RegistryHelper.handleRegistrar(FARLANDER_VILLAGE, event.getRegistry());
		JigsawAccessHelper.addIllagerStructures(SMALL_HOUSE.getStructure(), FARLANDER_VILLAGE.getStructure());
	}
}
