package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.boss.summon.EnderColossusShadowEntity;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class EnderGolemModel<T extends Entity> extends SegmentedModel<T>
{
	// fields
	public ModelRenderer headwear;
	ModelRenderer LowerBody;
	public ModelRenderer RightArm;
	ModelRenderer LeftArm;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer LowerWaist;
	ModelRenderer UpperWaist;
	ModelRenderer UpperBody;
	ModelRenderer Neck;
	public ModelRenderer headSet, RightUpperHorn, LeftUpperHorn, RightLowerHorn, LeftLowerHorn;
	public boolean isAttacking = false;
	public int attackTimer;

	public EnderGolemModel()
	{
		textureWidth = 64;
		textureHeight = 64;

		headwear = new ModelRenderer(this, 0, 21);
		headwear.addBox(-4F, -8F, -4F, 7, 8, 7);
		headwear.setRotationPoint(1F, -28F, 1F);
		headwear.setTextureSize(64, 64);
		setRotation(headwear, 0F, 0F, 0F);
		LowerBody = new ModelRenderer(this, 40, 54);
		LowerBody.addBox(-4F, 0F, -2F, 9, 7, 3);
		LowerBody.setRotationPoint(0F, -20F, 1F);
		LowerBody.setTextureSize(64, 64);
		setRotation(LowerBody, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 52, 0);
		RightArm.addBox(-1F, -2F, -1F, 3, 35, 3);
		RightArm.setRotationPoint(-7F, -23F, 0F);
		RightArm.setTextureSize(64, 64);
		setRotation(RightArm, 0F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 52, 0);
		LeftArm.addBox(-1F, -2F, -1F, 3, 35, 3);
		LeftArm.setRotationPoint(7F, -23F, 0F);
		LeftArm.setTextureSize(64, 64);
		LeftArm.mirror = true;
		setRotation(LeftArm, 0F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 52, 0);
		RightLeg.addBox(-1F, 0F, -1F, 3, 31, 3);
		RightLeg.setRotationPoint(-3F, -7F, 0F);
		RightLeg.setTextureSize(64, 64);
		setRotation(RightLeg, 0F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 52, 0);
		LeftLeg.addBox(-1F, 0F, -1F, 3, 31, 3);
		LeftLeg.setRotationPoint(3F, -7F, 0F);
		LeftLeg.setTextureSize(64, 64);
		setRotation(LeftLeg, 0F, 0F, 0F);
		LowerWaist = new ModelRenderer(this, 40, 58);
		LowerWaist.addBox(0F, 0F, 0F, 9, 3, 3);
		LowerWaist.setRotationPoint(-4F, -10F, -1F);
		LowerWaist.setTextureSize(64, 64);
		setRotation(LowerWaist, 0F, 0F, 0F);
		UpperWaist = new ModelRenderer(this, 44, 56);
		UpperWaist.addBox(0F, 0F, 0F, 3, 3, 3);
		UpperWaist.setRotationPoint(-1F, -13F, -1F);
		UpperWaist.setTextureSize(64, 64);
		setRotation(UpperWaist, 0F, 0F, 0F);
		UpperBody = new ModelRenderer(this, 32, 53);
		UpperBody.addBox(0F, 0F, 0F, 11, 6, 5);
		UpperBody.setRotationPoint(-5F, -26F, -2F);
		UpperBody.setTextureSize(64, 64);
		setRotation(UpperBody, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 40, 57);
		Neck.addBox(0F, 0F, 0F, 3, 2, 3);
		Neck.setRotationPoint(-1F, -28F, -1F);
		Neck.setTextureSize(64, 64);
		setRotation(Neck, 0F, 0F, 0F);
		headSet = new ModelRenderer(this, 0, 0);
		headSet.setRotationPoint(0F, -29F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		headSet.addBox(-4F, -8F, -4F, 9, 9, 9);

		RightUpperHorn = new ModelRenderer(this, 43, 0);
		RightUpperHorn.setRotationPoint(0F, -29F, 0F);
		setRotation(RightUpperHorn, 0F, 0F, 0F);
		RightUpperHorn.addBox(-9F, -9F, 0F, 2, 5, 2);

		LeftUpperHorn = new ModelRenderer(this, 43, 0);
		LeftUpperHorn.setRotationPoint(0F, -29F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		LeftUpperHorn.addBox(8F, -9F, 0F, 2, 5, 2);

		LeftLowerHorn = new ModelRenderer(this, 8, 55);
		LeftLowerHorn.setRotationPoint(0F, -29F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		LeftLowerHorn.addBox(5F, -6F, 0F, 4, 2, 2);

		RightLowerHorn = new ModelRenderer(this, 8, 55);
		RightLowerHorn.setRotationPoint(0F, -29F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		RightLowerHorn.addBox(-8F, -6F, 0F, 4, 2, 2);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.headwear, this.RightUpperHorn, this.LeftUpperHorn, this.LeftLowerHorn, this.RightLowerHorn, this.LowerBody, this.RightArm, this.LeftArm, this.RightLeg, this.LeftLeg, this.LowerWaist, this.UpperWaist, this.UpperBody, this.Neck, this.headSet);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T par7Entity, float f, float f1, float f2, float f3, float f4)
	{
		this.headSet.showModel = true;

		float speed = par7Entity instanceof EnderColossusShadowEntity ? 0.2262F : 0.5662F;
		RightLeg.rotateAngleX = MathHelper.cos(f * speed) * f1;
		LeftLeg.rotateAngleX = MathHelper.cos(f * speed + 3.141593F) * f1;
		RightLeg.rotateAngleY = 0.0F;
		LeftLeg.rotateAngleY = 0.0F;

		float var7 = -26.0F;

		this.headSet.rotateAngleY = f3 / 57.29578F;
		this.headSet.rotateAngleX = f4 / 57.29578F;
		this.headwear.rotateAngleY = f3 / 57.29578F;
		this.headwear.rotateAngleX = f4 / 57.29578F;

		this.headSet.rotationPointZ = -0.0F;
		this.headSet.rotationPointY = var7 - 3.0F;
		this.headwear.rotationPointZ = 1.0F;
		this.headwear.rotationPointY = var7 - 2.0F;

		if (this.isAttacking)
		{
			float var9 = 1.0F;
			this.headSet.rotationPointY -= var9 * 5.0F;
		}

		this.RightUpperHorn.rotateAngleY = this.headSet.rotateAngleY;
		this.RightUpperHorn.rotateAngleX = this.headSet.rotateAngleX;
		this.LeftUpperHorn.rotateAngleY = this.headSet.rotateAngleY;
		this.LeftUpperHorn.rotateAngleX = this.headSet.rotateAngleX;
		this.LeftLowerHorn.rotateAngleY = this.headSet.rotateAngleY;
		this.LeftLowerHorn.rotateAngleX = this.headSet.rotateAngleX;
		this.RightLowerHorn.rotateAngleY = this.headSet.rotateAngleY;
		this.RightLowerHorn.rotateAngleX = this.headSet.rotateAngleX;

		this.RightUpperHorn.rotationPointY = this.headSet.rotationPointY;
		this.LeftUpperHorn.rotationPointY = this.headSet.rotationPointY;
		this.LeftLowerHorn.rotationPointY = this.headSet.rotationPointY;
		this.RightLowerHorn.rotationPointY = this.headSet.rotationPointY;

	}

	@Override
	public void setLivingAnimations(T entityIn, float limbSwing, float limbSwingAmount, float partialTick)
	{
		if (entityIn instanceof EnderGolemEntity)
		{
			int i = ((EnderGolemEntity) entityIn).getAttackTimer();
			if (i > 0)
			{
				this.RightArm.rotateAngleX = -2.0F + 2.0F * this.triangleWave((float) i - partialTick, 10.0F);
				this.LeftArm.rotateAngleX = -2.0F + 2.0F * this.triangleWave((float) i - partialTick, 10.0F);
			}
			else
			{
				this.RightArm.rotateAngleX = (-0.2F + 1.2F * this.triangleWave(limbSwing, 13.0F)) * limbSwingAmount;
				this.LeftArm.rotateAngleX = (-0.2F - 1.2F * this.triangleWave(limbSwing, 13.0F)) * limbSwingAmount;
			}
		}

		if (entityIn instanceof EnderColossusShadowEntity)
		{
			int i = ((EnderColossusShadowEntity) entityIn).getAttackTimer();
			if (i > 0)
			{
				this.RightArm.rotateAngleX = -2.0F + 2.0F * this.triangleWave((float) i - partialTick, 10.0F);
				this.LeftArm.rotateAngleX = -2.0F + 2.0F * this.triangleWave((float) i - partialTick, 10.0F);
			}
			else
			{
				this.RightArm.rotateAngleX = (-0.2F + 1.5F * this.triangleWave(limbSwing, 13.0F)) * limbSwingAmount;
				this.LeftArm.rotateAngleX = (-0.2F - 1.5F * this.triangleWave(limbSwing, 13.0F)) * limbSwingAmount;
			}
		}

	}

	private float triangleWave(float p_78172_1_, float p_78172_2_)
	{
		return (Math.abs(p_78172_1_ % p_78172_2_ - p_78172_2_ * 0.5F) - p_78172_2_ * 0.25F) / (p_78172_2_ * 0.25F);
	}

}