package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;
import com.legacy.farlanders.entity.tameable.MysticEnderminionEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;

public class MysticEnderminionModel<T extends Entity> extends EnderminionModel<T>
{
	public ModelRenderer Head, Headwear;
	public ModelRenderer Body;
	public ModelRenderer LeftLeg1, LeftLeg2;
	public ModelRenderer RightLeg1, RightLeg2;
	public ModelRenderer CapeUpper, CapeUnder;

	public boolean shouldSwing;

	public MysticEnderminionModel()
	{
		textureWidth = 64;
		textureHeight = 32;

		Head = new ModelRenderer(this, 17, 19);
		Head.addBox(-3F, -6.533333F, -6F, 6, 6, 7);
		Head.setRotationPoint(0F, -8F, 3F);
		Head.setTextureSize(64, 32);
		setRotation(Head, 0F, 0F, 0F);
		Body = new ModelRenderer(this, 44, 20);
		Body.addBox(0F, 0F, 0F, 6, 8, 4);
		Body.setRotationPoint(-3F, -7F, 2F);
		Body.setTextureSize(64, 32);
		setRotation(Body, 0.6320364F, 0F, 0F);
		RightArm = new ModelRenderer(this, 56, 0);
		RightArm.addBox(-2F, 0F, -1F, 2, 17, 2);
		RightArm.setRotationPoint(-3F, -7F, 4F);
		RightArm.setTextureSize(64, 32);
		setRotation(RightArm, -0.1115358F, 0F, 0.0743572F);
		LeftArm = new ModelRenderer(this, 56, 0);
		LeftArm.addBox(0F, 0F, -1F, 2, 17, 2);
		LeftArm.setRotationPoint(3F, -7F, 4F);
		LeftArm.setTextureSize(64, 32);
		setRotation(LeftArm, -0.0743572F, 0F, -0.0743572F);
		LeftLeg1 = new ModelRenderer(this, 38, 0);
		LeftLeg1.addBox(0F, 0F, -1F, 2, 10, 2);
		LeftLeg1.setRotationPoint(-3F, -2F, 7F);
		LeftLeg1.setTextureSize(64, 32);
		setRotation(LeftLeg1, -0.2974289F, 0F, 0F);
		LeftLeg2 = new ModelRenderer(this, 47, 0);
		LeftLeg2.addBox(0F, 7.266667F, -6.6F, 2, 9, 2);
		LeftLeg2.setRotationPoint(-3F, -2F, 7F);
		LeftLeg2.setTextureSize(64, 32);
		setRotation(LeftLeg2, 0.3346075F, 0F, 0F);
		RightLeg1 = new ModelRenderer(this, 38, 0);
		RightLeg1.addBox(0F, 0F, -1F, 2, 10, 2);
		RightLeg1.setRotationPoint(1F, -2F, 7F);
		RightLeg1.setTextureSize(64, 32);
		setRotation(RightLeg1, -0.8922867F, 0F, 0F);
		RightLeg2 = new ModelRenderer(this, 47, 0);
		RightLeg2.addBox(0F, -3.533333F, -9.4F, 2, 9, 2);
		RightLeg2.setRotationPoint(1F, -2F, 7F);
		RightLeg2.setTextureSize(64, 32);
		setRotation(RightLeg2, 1.152537F, 0F, 0F);
		Headwear = new ModelRenderer(this, 0, 0);
		Headwear.addBox(-2.5F, -1.666667F, -5F, 5, 2, 5);
		Headwear.setRotationPoint(0F, -8F, 3F);
		Headwear.setTextureSize(64, 32);
		setRotation(Headwear, 0.2974289F, 0F, 0F);
		CapeUpper = new ModelRenderer(this, 0, 9);
		CapeUpper.addBox(0F, 0F, 0F, 8, 8, 0);
		CapeUpper.setRotationPoint(-4F, -9.266666F, 5.466667F);
		CapeUpper.setTextureSize(64, 32);
		setRotation(CapeUpper, 0.6320364F, 0F, 0F);
		CapeUnder = new ModelRenderer(this, 0, 17);
		CapeUnder.addBox(0F, 6.5F, 4.65F, 8, 15, 0);
		CapeUnder.setRotationPoint(-4F, -9.3F, 5.5F);
		CapeUnder.setTextureSize(64, 32);
		setRotation(CapeUnder, 0F, 0F, 0F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.Head, this.Headwear, this.Body, this.RightArm, this.LeftArm, this.RightLeg1, this.LeftLeg1, this.RightLeg2, this.LeftLeg2, this.CapeUpper, this.CapeUnder);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T entity, float f, float f1, float f2, float f3, float f4)
	{
		float onGround = 0;

		boolean isTamed = ((MysticEnderminionEntity) entity).isTamed();

		CapeUpper.rotateAngleX = 0.62F + f1 * 0.6F;

		CapeUnder.rotateAngleX = f1 * 0.6F;

		Head.rotateAngleY = f3 / 57.29578F;
		Head.rotateAngleX = f4 / 57.29578F;
		Headwear.rotateAngleY = f3 / 57.29578F;
		Headwear.rotateAngleX = f4 / 57.29578F;
		float var7 = MathHelper.sin((onGround - 1.03F) * (float) Math.PI);
		float var8 = MathHelper.sin((1.0F - (1.0F - onGround - 0.5F) * (1.0F - onGround - 0.5F)) * (float) Math.PI);

		if (entity.getMotion() != Vector3d.ZERO)
		{
			LeftLeg1.rotateAngleX = f1 - 0.35f;
			LeftLeg1.rotateAngleZ = 0.0F;
			LeftLeg1.rotateAngleY = 0.0F;
			LeftLeg2.rotateAngleX = f1 + 0.28f;
			LeftLeg2.rotateAngleZ = 0.0F;
			LeftLeg2.rotateAngleY = 0.0F;
			RightLeg1.rotateAngleX = f1 - 0.76f;
			RightLeg1.rotateAngleZ = 0.0F;
			RightLeg1.rotateAngleY = 0.0F;
			RightLeg2.rotateAngleX = f1 + 1.26f;
			RightLeg2.rotateAngleZ = 0.0F;
			RightLeg2.rotateAngleY = 0.0F;
		}
		else
		{
			this.LeftLeg1.rotateAngleZ = 0.01F;
			this.LeftLeg1.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.LeftLeg1.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.LeftLeg1.rotateAngleX -= var7 * 1.2F - var8 * (-0.75F);
			this.LeftLeg1.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.016F;
			this.LeftLeg1.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;
			this.LeftLeg2.rotateAngleZ = 0.01F;
			this.LeftLeg2.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.LeftLeg2.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.LeftLeg2.rotateAngleX -= var7 * 1.2F - var8 * (0.14F);
			this.LeftLeg2.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.016F;
			this.LeftLeg2.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;

			this.RightLeg1.rotateAngleZ = 0.01F;
			this.RightLeg1.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.RightLeg1.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.RightLeg1.rotateAngleX -= var7 * 1.2F - var8 * (-1.33F);
			this.RightLeg1.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.005F;
			this.RightLeg1.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.19F;
			this.RightLeg2.rotateAngleZ = 0.01F;
			this.RightLeg2.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.RightLeg2.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.RightLeg2.rotateAngleX -= var7 * 1.2F - var8 * (1.55F);
			this.RightLeg2.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.005F;
			this.RightLeg2.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.19F;
		}

		this.LeftArm.rotateAngleZ = -0.08F;
		this.LeftArm.rotateAngleY = -(0.1F - var7 * 0.6F);
		this.LeftArm.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
		this.LeftArm.rotateAngleX -= var7 * 1.2F - var8 * (-0.2F);
		this.LeftArm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.050F;
		this.LeftArm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.03F;

		CapeUpper.rotateAngleX = 0.62F + f1 * 0.6F;

		CapeUnder.rotateAngleX = f1 * 0.6F;

		if (isTamed)
			Headwear.rotateAngleX = (f4 + 10.0f) / 57.29578F;

		if (this.swingProgress > 0)
		{
			if (this.RightArm.rotateAngleX > (0.05f + MathHelper.cos(2.567F) * 0.35f))
				this.RightArm.rotateAngleX = 0.05f + MathHelper.cos(2.567F) * 0.35f;
			else
				this.RightArm.rotateAngleX += 0.05f + MathHelper.cos(2.567F) * 0.35f;
		}
		else
		{
			this.RightArm.rotateAngleZ = 0.08F;
			this.RightArm.rotateAngleY = -(0.1F - var7 * 0.6F);
			this.RightArm.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
			this.RightArm.rotateAngleX -= var7 * 1.2F - var8 * (-0.2F);
			this.RightArm.rotateAngleZ -= MathHelper.cos(f2 * 0.09F) * 0.050F;
			this.RightArm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.03F;
		}
	}

	@Override
	public void translateHand(HandSide side, MatrixStack matrixStackIn)
	{
		this.getArmForSide(side).translateRotate(matrixStackIn);
	}
}