package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.model.IHasArm;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.MathHelper;

public class FarlanderModel<T extends Entity> extends SegmentedModel<T> implements IHasArm
{
	// fields
	public ModelRenderer Head;
	ModelRenderer Headwear;
	ModelRenderer Body;
	public ModelRenderer RightArm;
	ModelRenderer LeftArm;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer Neck;

	/** Is the enderman attacking an entity? */
	public boolean isAttacking;
	public boolean hasSword;

	public FarlanderModel()
	{
		textureWidth = 64;
		textureHeight = 32;

		Head = new ModelRenderer(this, 0, 0);
		Head.addBox(-4F, -8F, -4F, 8, 8, 8);
		Head.setRotationPoint(0F, 0F, 0F);
		Head.setTextureSize(64, 32);
		setRotation(Head, 0F, 0F, 0F);
		Headwear = new ModelRenderer(this, 0, 16);
		Headwear.addBox(-4F, -8F, -4F, 8, 8, 8);
		Headwear.setRotationPoint(0F, 0F, 0F);
		Headwear.setTextureSize(64, 32);
		setRotation(Headwear, 0F, 0F, 0F);
		Body = new ModelRenderer(this, 32, 16);
		Body.addBox(-4F, 0F, -2F, 8, 6, 4);
		Body.setRotationPoint(0F, 1F, 0F);
		Body.setTextureSize(64, 32);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 56, 0);
		RightArm.addBox(-1F, -2F, -1F, 2, 19, 2);
		RightArm.setRotationPoint(-5F, 3F, 0F);
		RightArm.setTextureSize(64, 32);
		setRotation(RightArm, 0F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 56, 0);
		LeftArm.addBox(-1F, -2F, -1F, 2, 19, 2);
		LeftArm.setRotationPoint(5F, 3F, 0F);
		LeftArm.setTextureSize(64, 32);
		setRotation(LeftArm, 0F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 56, 0);
		RightLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		RightLeg.setRotationPoint(-2F, 7F, 0F);
		RightLeg.setTextureSize(64, 32);
		setRotation(RightLeg, 0F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 56, 0);
		LeftLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		LeftLeg.setRotationPoint(2F, 7F, 0F);
		LeftLeg.setTextureSize(64, 32);
		setRotation(LeftLeg, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 8, 3);
		Neck.addBox(0F, 0F, 0F, 2, 1, 2);
		Neck.setRotationPoint(-1F, 0F, -1F);
		Neck.setTextureSize(64, 32);
		setRotation(Neck, 0F, 0F, 0F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.Head, this.Headwear, this.Body, this.RightArm, this.LeftArm, this.RightLeg, this.LeftLeg, this.Neck);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T par7Entity, float f, float f1, float f2, float f3, float f4)
	{
		float onGround = 0;
		Head.rotateAngleY = f3 / 57.29578F;
		Head.rotateAngleX = f4 / 57.29578F;
		Headwear.rotateAngleY = f3 / 57.29578F;
		Headwear.rotateAngleX = f4 / 57.29578F;
		LeftArm.rotateAngleX = f1;
		LeftArm.rotateAngleZ = 0.0F;
		RightLeg.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
		LeftLeg.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * f1;
		RightLeg.rotateAngleY = 0.0F;
		LeftLeg.rotateAngleY = 0.0F;

		if (((LivingEntity) par7Entity).getHeldItemMainhand() != ItemStack.EMPTY)
		{
			this.RightArm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * 2.0F * f1 * 0.5F;
			this.RightArm.rotateAngleZ = 0.0F;
			this.RightArm.rotateAngleX = this.RightArm.rotateAngleX * 0.5F - ((float) Math.PI / 10F) * (float) 0;
			this.RightArm.rotateAngleY = 0.0F;
			float var8;
			float var9;

			if (onGround > -9990.0F)
			{
				var8 = onGround;
				this.Body.rotateAngleY = MathHelper.sin(MathHelper.sqrt(var8) * (float) Math.PI * 2.0F) * 0.2F;
				this.RightArm.rotationPointZ = MathHelper.sin(this.Body.rotateAngleY) * 5.0F;
				this.RightArm.rotationPointX = -MathHelper.cos(this.Body.rotateAngleY) * 5.0F;
				this.RightArm.rotateAngleY += this.Body.rotateAngleY;
				var8 = 1.0F - onGround;
				var8 *= var8;
				var8 *= var8;
				var8 = 1.0F - var8;
				var9 = MathHelper.sin(var8 * (float) Math.PI);
				float var10 = MathHelper.sin(onGround * (float) Math.PI) * -(this.Head.rotateAngleX - 0.7F) * 0.75F;
				this.RightArm.rotateAngleX = (float) ((double) this.RightArm.rotateAngleX - ((double) var9 * 1.2D + (double) var10));
				this.RightArm.rotateAngleY += this.Body.rotateAngleY * 2.0F;
				this.RightArm.rotateAngleZ = MathHelper.sin(onGround * (float) Math.PI) * -0.4F;
			}

			this.RightArm.rotateAngleZ += MathHelper.cos(f3 * 0.09F) * 0.05F + 0.05F;
			this.RightArm.rotateAngleX += MathHelper.sin(f3 * 0.067F) * 0.05F;

			if (par7Entity instanceof LooterEntity)
			{
				this.Head.rotationPointY = -5.0F;
			}
		}
		else
		{
			RightArm.rotateAngleX = f1;
			RightArm.rotateAngleZ = 0.0F;
			this.Head.rotationPointY = 0.0f;
		}
	}

	@Override
	public void translateHand(HandSide side, MatrixStack matrixStackIn)
	{
		this.getArmForSide(side).translateRotate(matrixStackIn);
	}

	protected ModelRenderer getArmForSide(HandSide side)
	{
		return side == HandSide.LEFT ? this.LeftArm : this.RightArm;
	}
}