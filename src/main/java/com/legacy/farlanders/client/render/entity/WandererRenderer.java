package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.WandererEyesLayer;
import com.legacy.farlanders.client.render.model.WandererModel;
import com.legacy.farlanders.entity.WandererEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class WandererRenderer extends MobRenderer<WandererEntity, WandererModel<WandererEntity>>
{
    private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/wanderer.png");

	public WandererRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new WandererModel<>(), 0.5F);
		this.addLayer(new WandererEyesLayer<>(this));
	}

	@Override
	protected void preRenderCallback(WandererEntity entitylivingbaseIn, MatrixStack matrixStackIn, float partialTickTime)
	{
		float f1 = 0.9375F;
		matrixStackIn.scale(f1, f1, f1);
	}

	public ResourceLocation getEntityTexture(WandererEntity entity)
	{
		return TEXTURE;
	}


}