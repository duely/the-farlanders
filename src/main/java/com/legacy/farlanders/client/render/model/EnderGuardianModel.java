package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.model.IHasArm;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.MathHelper;

public class EnderGuardianModel<T extends Entity> extends SegmentedModel<T> implements IHasArm
{
	// fields
	ModelRenderer Body;
	public ModelRenderer RightArm;
	ModelRenderer LeftArm;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer Neck;
	ModelRenderer BodyLower;
	public ModelRenderer Head, Headwear, LeftLowerHorn, LeftUpperHorn, RightLowerHorn, RightUpperHorn;
	ModelRenderer Quiver;
	public boolean isAttacking;

	public EnderGuardianModel()
	{
		textureWidth = 64;
		textureHeight = 32;

		Head = new ModelRenderer(this, 0, 0);
		Head.setRotationPoint(0F, 0F, 0F);
		setRotation(Head, 0F, 0F, 0F);
		Head.addBox(-4F, -8F, -4F, 8, 8, 8);
		Headwear = new ModelRenderer(this, 0, 16);
		Headwear.setRotationPoint(0F, 0F, 0F);
		setRotation(Headwear, 0F, 0F, 0F);
		Headwear.addBox(-4F, -8F, -4F, 8, 8, 8);
		LeftLowerHorn = new ModelRenderer(this, 44, 27);
		LeftLowerHorn.setRotationPoint(0F, 0F, 0F);
		setRotation(LeftLowerHorn, 0F, 0F, 0F);
		LeftLowerHorn.addBox(4F, -6F, -1F, 2, 2, 2);
		LeftUpperHorn = new ModelRenderer(this, 53, 25);
		LeftUpperHorn.setRotationPoint(0F, 0F, 0F);
		setRotation(LeftUpperHorn, 0F, 0F, 0F);
		LeftUpperHorn.addBox(6F, -8F, -1F, 2, 4, 2);
		RightLowerHorn = new ModelRenderer(this, 48, 25);
		RightLowerHorn.setRotationPoint(0F, 0F, 0F);
		setRotation(RightLowerHorn, 0F, 0F, 0F);
		RightLowerHorn.addBox(-6F, -6F, -1F, 2, 2, 2);
		RightUpperHorn = new ModelRenderer(this, 53, 21);
		RightUpperHorn.setRotationPoint(0F, 0F, 0F);
		setRotation(RightUpperHorn, 0F, 0F, 0F);
		RightUpperHorn.addBox(-8F, -8F, -1F, 2, 4, 2);
		Body = new ModelRenderer(this, 32, 16);
		Body.addBox(-4F, 0F, -2F, 8, 4, 4);
		Body.setRotationPoint(0F, 1F, 0F);
		Body.setTextureSize(64, 32);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 56, 0);
		RightArm.addBox(-1F, -2F, -1F, 2, 17, 2);
		RightArm.setRotationPoint(-5F, 3F, 0F);
		RightArm.setTextureSize(64, 32);
		setRotation(RightArm, 0F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 56, 0);
		LeftArm.addBox(-1F, -2F, -1F, 2, 17, 2);
		LeftArm.setRotationPoint(5F, 3F, 0F);
		LeftArm.setTextureSize(64, 32);
		setRotation(LeftArm, 0F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 56, 0);
		RightLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		RightLeg.setRotationPoint(-2F, 7F, 0F);
		RightLeg.setTextureSize(64, 32);
		setRotation(RightLeg, 0F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 56, 0);
		LeftLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		LeftLeg.setRotationPoint(2F, 7F, 0F);
		LeftLeg.setTextureSize(64, 32);
		setRotation(LeftLeg, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 8, 3);
		Neck.addBox(0F, 0F, 0F, 2, 1, 2);
		Neck.setRotationPoint(-1F, 0F, -1F);
		Neck.setTextureSize(64, 32);
		setRotation(Neck, 0F, 0F, 0F);
		BodyLower = new ModelRenderer(this, 37, 25);
		BodyLower.addBox(0F, 0F, 0F, 2, 4, 2);
		BodyLower.setRotationPoint(-1F, 5F, -1F);
		BodyLower.setTextureSize(64, 32);
		setRotation(BodyLower, 0F, 0F, 0F);
		Quiver = new ModelRenderer(this, 40, 0);
		Quiver.addBox(0F, 0F, 0F, 3, 10, 3);
		Quiver.setRotationPoint(-4F, 2F, 2F);
		Quiver.setTextureSize(64, 32);
		setRotation(Quiver, 0F, 0F, -0.4089647F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.Head, this.Headwear, this.LeftLowerHorn, this.LeftUpperHorn, this.RightLowerHorn, this.RightUpperHorn, this.Body, this.RightArm, this.LeftArm, this.RightLeg, this.LeftLeg, this.Neck, this.BodyLower, this.Quiver);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T par7entity, float f, float f1, float f2, float f3, float f4)
	{
		Head.rotateAngleY = f3 / 57.29578F;
		Head.rotateAngleX = f4 / 57.29578F;
		RightLeg.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
		LeftLeg.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * f1;
		RightLeg.rotateAngleY = 0.0F;
		LeftLeg.rotateAngleY = 0.0F;
		this.RightArm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * 2.0F * f1 * 0.5F;
		this.LeftArm.rotateAngleX = MathHelper.cos(f * 0.6662F) * 2.0F * f1 * 0.5F;
		this.RightArm.rotateAngleZ = 0.0F;
		this.LeftArm.rotateAngleZ = 0.0F;
		Quiver.rotateAngleX = f1;

		Headwear.rotateAngleY = Head.rotateAngleY;
		Headwear.rotateAngleX = Head.rotateAngleX;
		LeftLowerHorn.rotateAngleY = Head.rotateAngleY;
		LeftLowerHorn.rotateAngleX = Head.rotateAngleX;
		LeftUpperHorn.rotateAngleY = Head.rotateAngleY;
		LeftUpperHorn.rotateAngleX = Head.rotateAngleX;
		RightLowerHorn.rotateAngleY = Head.rotateAngleY;
		RightLowerHorn.rotateAngleX = Head.rotateAngleX;
		RightUpperHorn.rotateAngleY = Head.rotateAngleY;
		RightUpperHorn.rotateAngleX = Head.rotateAngleX;

		if (par7entity instanceof EnderGuardianEntity && par7entity != null)
		{
			if (((EnderGuardianEntity) par7entity).getAttacking() == true)
			{
				this.RightArm.rotateAngleZ = 0.0F;
				this.RightArm.rotateAngleX = -1.561502F;
				this.RightArm.rotateAngleY = -0.05F;
				this.LeftArm.rotateAngleZ = 0.0F;
				this.LeftArm.rotateAngleX = -1.561502F;
				this.LeftArm.rotateAngleY = 0.47F;
			}
		}
	}

	@Override
	public void translateHand(HandSide side, MatrixStack matrixStackIn)
	{
		this.getArmForSide(side).translateRotate(matrixStackIn);
	}

	protected ModelRenderer getArmForSide(HandSide side)
	{
		return side == HandSide.LEFT ? this.LeftArm : this.RightArm;
	}
}
