package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.model.IHasArm;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.MathHelper;

public class RebelModel<T extends Entity> extends SegmentedModel<T> implements IHasArm
{
	// fields
	public ModelRenderer Head;
	public ModelRenderer Headwear;
	ModelRenderer Body;
	public ModelRenderer RightArm;
	ModelRenderer LeftArm;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer Neck;
	public ModelRenderer helmet_1, helmet_2, helmet_3, helmet_4, helmet_5;

	public ModelRenderer horn_1, horn_2, horn_3, horn_4, horn_5, horn_6;

	public int heldItemRight;

	public RebelModel()
	{
		this.heldItemRight = 0;
		textureWidth = 128;
		textureHeight = 32;

		Head = new ModelRenderer(this, 0, 0);
		Head.addBox(-4F, -8F, -4F, 8, 8, 8);
		Head.setRotationPoint(0F, 0F, 0F);
		Head.setTextureSize(128, 32);
		setRotation(Head, 0F, 0F, 0F);
		Headwear = new ModelRenderer(this, 0, 16);
		Headwear.addBox(-4F, -8F, -4F, 8, 8, 8);
		Headwear.setRotationPoint(0F, 1.998401E-14F, 0F);
		Headwear.setTextureSize(128, 32);
		setRotation(Headwear, 0F, 0F, 0F);
		Body = new ModelRenderer(this, 33, 22);
		Body.addBox(-4F, 0F, -2F, 8, 6, 4);
		Body.setRotationPoint(0F, 1F, 0F);
		Body.setTextureSize(128, 32);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 34, 0);
		RightArm.addBox(-1F, -2F, -1F, 2, 19, 2);
		RightArm.setRotationPoint(-5F, 3F, 0F);
		RightArm.setTextureSize(128, 32);
		setRotation(RightArm, 0F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 34, 0);
		LeftArm.addBox(-1F, -2F, -1F, 2, 19, 2);
		LeftArm.setRotationPoint(5F, 3F, 0F);
		LeftArm.setTextureSize(128, 32);
		setRotation(LeftArm, 0F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 43, 0);
		RightLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		RightLeg.setRotationPoint(-2F, 7F, 0F);
		RightLeg.setTextureSize(128, 32);
		setRotation(RightLeg, 0F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 43, 0);
		LeftLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		LeftLeg.setRotationPoint(2F, 7F, 0F);
		LeftLeg.setTextureSize(128, 32);
		setRotation(LeftLeg, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 38, 27);
		Neck.addBox(0F, 0F, 0F, 2, 1, 2);
		Neck.setRotationPoint(-1F, 0F, -1F);
		Neck.setTextureSize(128, 32);
		setRotation(Neck, 0F, 0F, 0F);
		helmet_1 = new ModelRenderer(this, 110, 0);
		helmet_1.setRotationPoint(0F, 0F, 0F);
		setRotation(helmet_1, 0F, 0F, 0F);
		helmet_1.addBox(-4.533333F, -8.533334F, -4.533333F, 9, 7, 0);
		this.Head.addChild(this.helmet_1);

		helmet_2 = new ModelRenderer(this, 71, 0);
		helmet_2.setRotationPoint(0F, 0F, 0F);
		setRotation(helmet_2, 0F, 0F, 0F);
		helmet_2.addBox(-4.533333F, -8.466666F, -4.466667F, 0, 8, 9);
		this.Head.addChild(this.helmet_2);

		helmet_3 = new ModelRenderer(this, 110, 8);
		helmet_3.setRotationPoint(0F, 0F, 0F);
		setRotation(helmet_3, 0F, 0F, 0F);
		helmet_3.addBox(-4.533333F, -8.533334F, 4.533333F, 9, 8, 0);
		this.Head.addChild(this.helmet_3);

		helmet_4 = new ModelRenderer(this, 92, 23);
		helmet_4.setRotationPoint(0F, 0F, 0F);
		setRotation(helmet_4, 0F, 0F, 0F);
		helmet_4.addBox(-4.533333F, -8.466666F, -4.533333F, 9, 0, 9);
		this.Head.addChild(this.helmet_4);

		helmet_5 = new ModelRenderer(this, 90, 0);
		helmet_5.setRotationPoint(0F, 0F, 0F);
		setRotation(helmet_5, 0F, 0F, 0F);
		helmet_5.addBox(4.466667F, -8.533334F, -4.533333F, 0, 8, 9);
		this.Head.addChild(this.helmet_5);

		horn_1 = new ModelRenderer(this, 20, 9);
		horn_1.setRotationPoint(0F, 0F, 0F);
		setRotation(horn_1, 0F, 0F, 0F);
		horn_1.addBox(-8.5F, -6.5F, -0.5F, 4, 2, 2);
		this.Head.addChild(this.horn_1);

		horn_2 = new ModelRenderer(this, 24, 10);
		horn_2.setRotationPoint(0F, 0F, 0F);
		setRotation(horn_2, 0F, 0F, 0F);
		horn_2.addBox(-8.5F, -8.5F, -0.5F, 2, 2, 2);
		this.Head.addChild(this.horn_2);

		horn_3 = new ModelRenderer(this, 101, 25);
		horn_3.setRotationPoint(0F, 0F, 0F);
		setRotation(horn_3, 0F, 0F, 0F);
		horn_3.addBox(4F, -7F, -1F, 1, 3, 3);
		this.Head.addChild(this.horn_3);

		horn_4 = new ModelRenderer(this, 101, 25);
		horn_4.setRotationPoint(0F, 0F, 0F);
		setRotation(horn_4, 0F, 0F, 0F);
		horn_4.addBox(-5F, -7F, -1F, 1, 3, 3);
		this.Head.addChild(this.horn_4);

		horn_5 = new ModelRenderer(this, 24, 9);
		horn_5.setRotationPoint(0F, 0F, 0F);
		setRotation(horn_5, 0F, 0F, 0F);
		horn_5.addBox(6.533333F, -8.533334F, -0.5333334F, 2, 2, 2);
		this.Head.addChild(this.horn_5);

		horn_6 = new ModelRenderer(this, 20, 9);
		horn_6.setRotationPoint(0F, 0F, 0F);
		setRotation(horn_6, 0F, 0F, 0F);
		horn_6.addBox(4.533333F, -6.533333F, -0.5333334F, 4, 2, 2);
		this.Head.addChild(this.horn_6);

	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.Head, this.Headwear, this.Body, this.RightArm, this.LeftArm, this.RightLeg, this.LeftLeg, this.Neck);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T e, float f, float f1, float f2, float f3, float f4)
	{
		float onGround = 0.0F;
		Head.rotateAngleY = f3 / 57.29578F;
		Head.rotateAngleX = f4 / 57.29578F;
		
		this.Headwear.copyModelAngles(this.Head);

		RightLeg.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
		LeftLeg.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * f1;
		RightLeg.rotateAngleY = 0.0F;
		LeftLeg.rotateAngleY = 0.0F;
		LeftArm.rotateAngleX = f1;
		LeftArm.rotateAngleZ = 0.0F;

		this.RightArm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * 2.0F * f1 * 0.5F;
		this.RightArm.rotateAngleZ = 0.0F;

		if (((RebelEntity) e).getHeldItemMainhand() != ItemStack.EMPTY)
		{
			this.getArmForSide(((RebelEntity) e).getPrimaryHand()).rotateAngleX = this.getArmForSide(((RebelEntity) e).getPrimaryHand()).rotateAngleX * 0.5F - ((float) Math.PI / 10F) * (float) this.heldItemRight;
		}

		this.RightArm.rotateAngleY = 0.0F;
		float var8;
		float var9;

		if (onGround > -9990.0F)
		{
			var8 = onGround;
			this.Body.rotateAngleY = MathHelper.sin(MathHelper.sqrt(var8) * (float) Math.PI * 2.0F) * 0.2F;
			this.RightArm.rotationPointZ = MathHelper.sin(this.Body.rotateAngleY) * 5.0F;
			this.RightArm.rotationPointX = -MathHelper.cos(this.Body.rotateAngleY) * 5.0F;
			this.RightArm.rotateAngleY += this.Body.rotateAngleY;
			var8 = 1.0F - onGround;
			var8 *= var8;
			var8 *= var8;
			var8 = 1.0F - var8;
			var9 = MathHelper.sin(var8 * (float) Math.PI);
			float var10 = MathHelper.sin(onGround * (float) Math.PI) * -(this.Head.rotateAngleX - 0.7F) * 0.75F;
			this.RightArm.rotateAngleX = (float) ((double) this.RightArm.rotateAngleX - ((double) var9 * 1.2D + (double) var10));
			this.RightArm.rotateAngleY += this.Body.rotateAngleY * 2.0F;
			this.RightArm.rotateAngleZ = MathHelper.sin(onGround * (float) Math.PI) * -0.4F;
		}

		this.RightArm.rotateAngleZ += MathHelper.cos(f3 * 0.09F) * 0.05F + 0.05F;
		this.RightArm.rotateAngleX += MathHelper.sin(f3 * 0.067F) * 0.05F;

		/*helmet_2.rotateAngleY = Head.rotateAngleY;
		helmet_2.rotateAngleX = Head.rotateAngleX;
		helmet_3.rotateAngleY = Head.rotateAngleY;
		helmet_3.rotateAngleX = Head.rotateAngleX;
		helmet_4.rotateAngleY = Head.rotateAngleY;
		helmet_4.rotateAngleX = Head.rotateAngleX;
		helmet_5.rotateAngleY = Head.rotateAngleY;
		helmet_5.rotateAngleX = Head.rotateAngleX;
		
		horn_1.rotateAngleY = Head.rotateAngleY;
		horn_1.rotateAngleX = Head.rotateAngleX;
		horn_2.rotateAngleY = Head.rotateAngleY;
		horn_2.rotateAngleX = Head.rotateAngleX;
		horn_3.rotateAngleY = Head.rotateAngleY;
		horn_3.rotateAngleX = Head.rotateAngleX;
		horn_4.rotateAngleY = Head.rotateAngleY;
		horn_4.rotateAngleX = Head.rotateAngleX;
		horn_5.rotateAngleY = Head.rotateAngleY;
		horn_5.rotateAngleX = Head.rotateAngleX;
		horn_6.rotateAngleY = Head.rotateAngleY;
		horn_6.rotateAngleX = Head.rotateAngleX;*/
	}

	@Override
	public void translateHand(HandSide side, MatrixStack matrixStackIn)
	{
		this.getArmForSide(side).translateRotate(matrixStackIn);
	}

	protected ModelRenderer getArmForSide(HandSide side)
	{
		return side == HandSide.LEFT ? this.LeftArm : this.RightArm;
	}
}