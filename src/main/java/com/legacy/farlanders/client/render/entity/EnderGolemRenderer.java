package com.legacy.farlanders.client.render.entity;

import java.util.Random;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.GolemEyeLayer;
import com.legacy.farlanders.client.render.model.EnderGolemModel;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;

public class EnderGolemRenderer extends MobRenderer<EnderGolemEntity, EnderGolemModel<EnderGolemEntity>>
{
	private final Random rnd = new Random();

	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/golem/ender_golem.png");

	public EnderGolemRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new EnderGolemModel<>(), 0.5F);
		this.addLayer(new GolemEyeLayer(this));
	}

	@Override
	public void render(EnderGolemEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
	{
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		EnderGolemModel<EnderGolemEntity> golemmodel = this.getEntityModel();
		golemmodel.isAttacking = entityIn.getAngry();
	}

	@Override
	public Vector3d getRenderOffset(EnderGolemEntity entityIn, float partialTicks)
	{
		if (entityIn.getAngry())
		{
			return new Vector3d(this.rnd.nextGaussian() * 0.02D, 0.0D, this.rnd.nextGaussian() * 0.02D);
		}
		else
		{
			return super.getRenderOffset(entityIn, partialTicks);
		}
	}

	public ResourceLocation getEntityTexture(EnderGolemEntity entity)
	{
		return TEXTURE;
	}
}