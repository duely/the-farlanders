package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.hostile.boss.summon.ThrownBlockEntity;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ThrownBlockRenderer extends EntityRenderer<ThrownBlockEntity>
{
	public ThrownBlockRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
		this.shadowSize = 0.5F;
	}

	/*public void doRender(ThrownBlockEntity entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		GlStateManager.pushMatrix();
		GlStateManager.color3f(1.5F, 1.5F, 1.5F);	
		GlStateManager.translated(x, y + 1.5D, z);
	    GlStateManager.scalef(3.0F, 3.0F, 3.0F);
		this.bindTexture(TheFarlandersMod.locate("textures/entity/thrown_block.png"));
		new BlockEntityModel<ThrownBlockEntity>().render(entity, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
		GlStateManager.popMatrix();
	}*/

	public ResourceLocation getEntityTexture(ThrownBlockEntity entity)
	{
		return TheFarlandersMod.locate("textures/entity/thrown_block.png");
	}
}