package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class WandererModel<T extends Entity> extends SegmentedModel<T>
{
	// fields
	ModelRenderer Head;
	ModelRenderer Headwear;
	ModelRenderer Body;
	ModelRenderer RightArm;
	ModelRenderer LeftDownArm;
	ModelRenderer LeftArm;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer Neck;
	ModelRenderer Bag;
	ModelRenderer Stick;

	public WandererModel()
	{
		textureWidth = 128;
		textureHeight = 32;

		Head = new ModelRenderer(this, 0, 0);
		Head.addBox(-4F, -8F, -4F, 8, 8, 8);
		Head.setRotationPoint(0F, 0F, 0F);
		Head.setTextureSize(128, 32);
		setRotation(Head, 0F, 0F, 0F);
		Headwear = new ModelRenderer(this, 0, 16);
		Headwear.addBox(-4F, -8F, -4F, 8, 8, 8);
		Headwear.setRotationPoint(0F, 0F, 0F);
		Headwear.setTextureSize(128, 32);
		setRotation(Headwear, 0F, 0F, 0F);
		Body = new ModelRenderer(this, 33, 22);
		Body.addBox(-4F, 0F, -2F, 8, 6, 4);
		Body.setRotationPoint(0F, 1F, 0F);
		Body.setTextureSize(128, 32);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 56, 0);
		RightArm.addBox(-1F, -2F, -1F, 2, 19, 2);
		RightArm.setRotationPoint(-5F, 3F, 0F);
		RightArm.setTextureSize(128, 32);
		setRotation(RightArm, 0F, 0F, 0F);
		LeftDownArm = new ModelRenderer(this, 56, 11);
		LeftDownArm.addBox(-1F, -1.666667F, 5.133333F, 2, 8, 2);
		LeftDownArm.setRotationPoint(5F, 3F, 9.992007E-15F);
		LeftDownArm.setTextureSize(128, 32);
		setRotation(LeftDownArm, -2.007645F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 56, 0);
		LeftArm.addBox(-1F, -2F, -1F, 2, 9, 2);
		LeftArm.setRotationPoint(5F, 3F, 0F);
		LeftArm.setTextureSize(128, 32);
		setRotation(LeftArm, -0.2974289F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 69, 0);
		RightLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		RightLeg.setRotationPoint(-2F, 7F, 0F);
		RightLeg.setTextureSize(128, 32);
		setRotation(RightLeg, 0F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 69, 0);
		LeftLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		LeftLeg.setRotationPoint(2F, 7F, 0F);
		LeftLeg.setTextureSize(128, 32);
		setRotation(LeftLeg, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 56, 3);
		Neck.addBox(0F, 0F, 0F, 2, 1, 2);
		Neck.setRotationPoint(-1F, 0F, -1F);
		Neck.setTextureSize(128, 32);
		setRotation(Neck, 0F, 0F, 0F);
		Bag = new ModelRenderer(this, 33, 1);
		Bag.addBox(-2F, -3F, 9F, 5, 9, 5);
		Bag.setRotationPoint(5F, 3F, 0F);
		Bag.setTextureSize(128, 32);
		setRotation(Bag, 0.3717861F, 0F, 0F);
		Stick = new ModelRenderer(this, 65, 0);
		Stick.addBox(0F, -9F, 1.6F, 1, 21, 1);
		Stick.setRotationPoint(5F, 3F, 0F);
		Stick.setTextureSize(128, 32);
		setRotation(Stick, 2.063413F, 0F, 0F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.Head, this.Headwear, this.Body, this.RightArm, this.LeftArm, this.LeftDownArm, this.RightLeg, this.LeftLeg, this.Neck, this.Bag, this.Stick);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(T par7Entity, float f, float f1, float f2, float f3, float f4)
	{
		float onGround = 0;

		Head.rotateAngleY = f3 / 57.29578F;
		Head.rotateAngleX = f4 / 57.29578F;
		Headwear.rotateAngleY = f3 / 57.29578F;
		Headwear.rotateAngleX = f4 / 57.29578F;
		RightArm.rotateAngleX = f1;
		RightArm.rotateAngleZ = 0.0F;
		RightLeg.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
		LeftLeg.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * f1;
		RightLeg.rotateAngleY = 0.0F;
		LeftLeg.rotateAngleY = 0.0f;

		float var7 = MathHelper.sin((onGround - 1.03F) * (float) Math.PI);
		float var8 = MathHelper.sin((1.0F - (1.0F - onGround - 0.5F) * (1.0F - onGround - 0.5F)) * (float) Math.PI);
		this.Bag.rotateAngleZ = -0.07F;
		this.Bag.rotateAngleY = -(0.1F - var7 * 0.6F);
		this.Bag.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
		this.Bag.rotateAngleX -= var7 * 1.2F - var8 * 0.43F;
		this.Bag.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.035F;
		this.Bag.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.15F;
		this.Stick.rotateAngleZ = -0.07F;
		this.Stick.rotateAngleY = -(0.1F - var7 * 0.6F);
		this.Stick.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
		this.Stick.rotateAngleX -= var7 * 1.2F - var8 * 2.78F;
		this.Stick.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.035F;
		this.Stick.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.15F;
		this.LeftArm.rotateAngleZ = -0.07F;
		this.LeftArm.rotateAngleY = -(0.1F - var7 * 0.6F);
		this.LeftArm.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
		this.LeftArm.rotateAngleX -= var7 * 1.2F - var8 * (-0.5F);
		this.LeftArm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.035F;
		this.LeftArm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.15F;
		this.LeftDownArm.rotateAngleZ = -0.07F;
		this.LeftDownArm.rotateAngleY = -(0.1F - var7 * 0.6F);
		this.LeftDownArm.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F);
		this.LeftDownArm.rotateAngleX -= var7 * 1.2F - var8 * 6.0F;
		this.LeftDownArm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.035F;
		this.LeftDownArm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.15F;
	}
}