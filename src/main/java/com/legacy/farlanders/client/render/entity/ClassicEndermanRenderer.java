package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.eyes.ClassicEyesLayer;
import com.legacy.farlanders.entity.hostile.ClassicEndermanEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.model.EndermanModel;
import net.minecraft.util.ResourceLocation;

public class ClassicEndermanRenderer extends MobRenderer<ClassicEndermanEntity, EndermanModel<ClassicEndermanEntity>>
{

	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/classic_enderman.png");

	public ClassicEndermanRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new EndermanModel<>(0.0F), 0.5F);
		this.addLayer(new ClassicEyesLayer<>(this));
		// this.addLayer(new HeldBlockLayer(this));
	}

	public ResourceLocation getEntityTexture(ClassicEndermanEntity entity)
	{
		return TEXTURE;
	}
}