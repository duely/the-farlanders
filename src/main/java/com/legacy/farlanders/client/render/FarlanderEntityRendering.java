package com.legacy.farlanders.client.render;

import com.legacy.farlanders.client.render.entity.ClassicEndermanRenderer;
import com.legacy.farlanders.client.render.entity.ElderFarlanderRenderer;
import com.legacy.farlanders.client.render.entity.EnderGolemRenderer;
import com.legacy.farlanders.client.render.entity.EnderGuardianRenderer;
import com.legacy.farlanders.client.render.entity.EnderminionRenderer;
import com.legacy.farlanders.client.render.entity.FanmadeEndermanRenderer;
import com.legacy.farlanders.client.render.entity.FarlanderRenderer;
import com.legacy.farlanders.client.render.entity.LooterRenderer;
import com.legacy.farlanders.client.render.entity.MysticEndermanRenderer;
import com.legacy.farlanders.client.render.entity.MysticEnderminionRenderer;
import com.legacy.farlanders.client.render.entity.RebelRenderer;
import com.legacy.farlanders.client.render.entity.TitanRenderer;
import com.legacy.farlanders.client.render.entity.WandererRenderer;
import com.legacy.farlanders.registry.FarlandersEntityTypes;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class FarlanderEntityRendering
{
	public static void init()
	{
		register(FarlandersEntityTypes.FARLANDER, FarlanderRenderer::new);
		register(FarlandersEntityTypes.ELDER_FARLANDER, ElderFarlanderRenderer::new);
		register(FarlandersEntityTypes.WANDERER, WandererRenderer::new);
		register(FarlandersEntityTypes.ENDER_GUARDIAN, EnderGuardianRenderer::new);
		register(FarlandersEntityTypes.LOOTER, LooterRenderer::new);
		register(FarlandersEntityTypes.REBEL, RebelRenderer::new);
		register(FarlandersEntityTypes.ENDERMINION, EnderminionRenderer::new);
		register(FarlandersEntityTypes.MYSTIC_ENDERMINION, MysticEnderminionRenderer::new);
		register(FarlandersEntityTypes.MYSTIC_ENDERMAN, MysticEndermanRenderer::new);
		register(FarlandersEntityTypes.CLASSIC_ENDERMAN, ClassicEndermanRenderer::new);
		register(FarlandersEntityTypes.FANMADE_ENDERMAN, FanmadeEndermanRenderer::new);
		register(FarlandersEntityTypes.ENDER_GOLEM, EnderGolemRenderer::new);
		register(FarlandersEntityTypes.TITAN, TitanRenderer::new);
		
		/*register(FarlandersEntityTypes.END_SPIRIT, EndSpiritRenderer::new);
		register(FarlandersEntityTypes.NIGHTFALL_SPIRIT, NightfallSpiritRenderer::new);
		
		register(FarlandersEntityTypes.ENDER_COLOSSUS, EnderColossusRenderer::new);
		register(FarlandersEntityTypes.MINI_ENDER_DRAGON, MiniDragonRenderer::new);
		register(FarlandersEntityTypes.ENDER_COLOSSUS_SHADOW, EnderColossusShadowRenderer::new);
		register(FarlandersEntityTypes.THROWN_BLOCK, ThrownBlockRenderer::new);*/
	}

	private static <T extends Entity> void register(EntityType<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}
}