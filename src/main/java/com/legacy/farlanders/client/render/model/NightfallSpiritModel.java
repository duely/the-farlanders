package com.legacy.farlanders.client.render.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class NightfallSpiritModel<T extends Entity> extends SegmentedModel<T>
{
	// fields
	ModelRenderer Body;
	public ModelRenderer RightArm;
	ModelRenderer LeftArm;
	ModelRenderer RightLeg;
	ModelRenderer LeftLeg;
	ModelRenderer Neck;
	ModelRenderer BodyLower;
	public ModelRenderer headSet, head, headwear, leftLowerHorn, leftUpperHorn, rightLowerHorn, rightUpperHorn;

	public NightfallSpiritModel()
	{
		textureWidth = 64;
		textureHeight = 32;

		headSet = new ModelRenderer(this);
		headSet.setRotationPoint(0F, 0F, 0F);
		setRotation(headSet, 0F, 0F, 0F);

		head = new ModelRenderer(this, 0, 0);
		head.addBox(-4F, -8F, -4F, 8, 8, 8);
		headSet.addChild(head);

		headwear = new ModelRenderer(this, 0, 16);
		headwear.addBox(-4F, -8F, -4F, 8, 8, 8);
		headSet.addChild(headwear);
		
		leftLowerHorn = new ModelRenderer(this, 44, 27);
		leftLowerHorn.addBox(4F, -6F, -1F, 2, 2, 2);
		headSet.addChild(leftLowerHorn);

		leftUpperHorn = new ModelRenderer(this, 53, 25);
		leftUpperHorn.addBox(6F, -8F, -1F, 2, 4, 2);
		headSet.addChild(leftUpperHorn);

		rightLowerHorn = new ModelRenderer(this, 48, 25);
		rightLowerHorn.addBox(-6F, -6F, -1F, 2, 2, 2);
		headSet.addChild(rightLowerHorn);

		rightUpperHorn = new ModelRenderer(this, 53, 21);
		rightUpperHorn.addBox(-8F, -8F, -1F, 2, 4, 2);
		headSet.addChild(rightUpperHorn);

		Body = new ModelRenderer(this, 32, 16);
		Body.addBox(-4F, 0F, -2F, 8, 4, 4);
		Body.setRotationPoint(0F, 1F, 0F);
		Body.setTextureSize(64, 32);
		setRotation(Body, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 56, 0);
		RightArm.addBox(-1F, -2F, -1F, 2, 17, 2);
		RightArm.setRotationPoint(-5F, 3F, 0F);
		RightArm.setTextureSize(64, 32);
		setRotation(RightArm, 0F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 56, 0);
		LeftArm.addBox(-1F, -2F, -1F, 2, 17, 2);
		LeftArm.setRotationPoint(5F, 3F, 0F);
		LeftArm.setTextureSize(64, 32);
		setRotation(LeftArm, 0F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 56, 0);
		RightLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		RightLeg.setRotationPoint(-2F, 7F, 0F);
		RightLeg.setTextureSize(64, 32);
		setRotation(RightLeg, 0F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 56, 0);
		LeftLeg.addBox(-1F, 0F, -1F, 2, 17, 2);
		LeftLeg.setRotationPoint(2F, 7F, 0F);
		LeftLeg.setTextureSize(64, 32);
		setRotation(LeftLeg, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 8, 3);
		Neck.addBox(0F, 0F, 0F, 2, 1, 2);
		Neck.setRotationPoint(-1F, 0F, -1F);
		Neck.setTextureSize(64, 32);
		setRotation(Neck, 0F, 0F, 0F);
		BodyLower = new ModelRenderer(this, 37, 25);
		BodyLower.addBox(0F, 0F, 0F, 2, 4, 2);
		BodyLower.setRotationPoint(-1F, 5F, -1F);
		BodyLower.setTextureSize(64, 32);
		setRotation(BodyLower, 0F, 0F, 0F);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.headSet, this.Body, this.BodyLower, this.RightArm, this.LeftArm, this.RightLeg, this.LeftLeg, this.Neck);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(T par7entity, float f, float f1, float f2, float f3, float f4)
	{
		headSet.rotateAngleY = f3 / 57.29578F;
		headSet.rotateAngleX = f4 / 57.29578F;
		LeftArm.rotateAngleX = f1;
		LeftArm.rotateAngleZ = 0.0F;
		RightArm.rotateAngleX = f1;
		RightArm.rotateAngleZ = 0.0F;
		RightLeg.rotateAngleX = f1;
		LeftLeg.rotateAngleX = f1;
		RightLeg.rotateAngleZ = 0.0F;
		LeftLeg.rotateAngleZ = 0.0F;
		this.headSet.rotationPointY = 0.0f;
	}
}
