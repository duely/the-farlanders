package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.model.NightfallSpiritModel;
import com.legacy.farlanders.entity.hostile.nightfall.NightfallSpiritEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class NightfallSpiritRenderer extends MobRenderer<NightfallSpiritEntity, NightfallSpiritModel<NightfallSpiritEntity>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/colossus/spirit.png");

	public NightfallSpiritRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new NightfallSpiritModel<>(), 0.0F);
	}

	/*@Override
	protected void renderModel(NightfallSpiritEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor)
	{
		GlStateManager.pushMatrix();
		this.bindEntityTexture(entity);
		
		GlStateManager.enableBlend();
		GlStateManager.disableAlphaTest();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ONE);
		GL11.glTranslatef(0, MathHelper.cos(ageInTicks / 6) / 10 - 0.2F, 0);
		
		if (entity.isInvisible())
		{
			GlStateManager.depthMask(false);
		}
		else
		{
			GlStateManager.depthMask(true);
		}
		
		int i = entity.getInvisValue(); // 61680
		int j = i % 65536;
		int k = i / 65536;
		GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, (float) j, (float) k);
		GlStateManager.color4f(2.0F, 2.0F, 2.0F, 1.0F);
		GameRenderer gamerenderer = Minecraft.getInstance().gameRenderer;
		gamerenderer.setupFogColor(true);
		this.entityModel.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
		gamerenderer.setupFogColor(false);
		i = entity.getBrightnessForRender();
		j = i % 65536;
		k = i / 65536;
		GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, (float) j, (float) k);
		GlStateManager.depthMask(true);
		GlStateManager.disableBlend();
		GlStateManager.enableAlphaTest();
		GlStateManager.popMatrix();
	}*/

	public ResourceLocation getEntityTexture(NightfallSpiritEntity entity)
	{
		return TEXTURE;
	}

}