package com.legacy.farlanders.client.render.entity.layer.eyes;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.model.ElderModel;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.AbstractEyesLayer;
import net.minecraft.entity.LivingEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ElderEyesLayer<T extends LivingEntity> extends AbstractEyesLayer<T, ElderModel<T>>
{
	private static final RenderType RENDER_TYPE = RenderType.getEyes(TheFarlandersMod.locate("textures/entity/farlander/elder_eyes.png"));

	public ElderEyesLayer(IEntityRenderer<T, ElderModel<T>> rendererIn)
	{
		super(rendererIn);
	}

	public RenderType getRenderType()
	{
		return RENDER_TYPE;
	}
}