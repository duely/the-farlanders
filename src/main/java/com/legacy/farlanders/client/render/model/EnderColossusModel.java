package com.legacy.farlanders.client.render.model;

import com.legacy.farlanders.entity.hostile.boss.EnderColossusEntity;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class EnderColossusModel<T extends EnderColossusEntity> extends SegmentedModel<T>
{
	// fields
	ModelRenderer Front1;
	ModelRenderer Front2;
	ModelRenderer Front3;
	ModelRenderer Front4;
	ModelRenderer Front5;
	ModelRenderer Front6;
	ModelRenderer Front7;
	ModelRenderer Front8;
	ModelRenderer Top1;
	ModelRenderer Top2;
	ModelRenderer Lateral1;
	ModelRenderer Lateral2;
	ModelRenderer Lateral3;
	ModelRenderer Lateral4;
	ModelRenderer Lateral5;
	ModelRenderer Lateral6;
	ModelRenderer Lateral7;
	ModelRenderer Lateral8;
	ModelRenderer Headwear;
	ModelRenderer RightArm;
	ModelRenderer RightArmDown;
	ModelRenderer LeftArm;
	ModelRenderer LeftArmDown;
	ModelRenderer RightLeg;
	ModelRenderer RightLegDown;
	ModelRenderer LeftLeg;
	ModelRenderer LeftLegDown;
	ModelRenderer UpperWaist;
	ModelRenderer Neck;
	ModelRenderer LowerWaist;
	ModelRenderer Back1;
	ModelRenderer Back2;
	ModelRenderer Back3;
	ModelRenderer Back4;
	ModelRenderer Back5;
	ModelRenderer Back6;
	ModelRenderer Back7;
	ModelRenderer Back8;
	ModelRenderer headSet, RightUpperHorn, LeftUpperHorn, RightLowerHorn, LeftLowerHorn;
	public boolean isAttacking = false;
	public int attackTimer, deathTicks;

	public EnderColossusModel()
	{
		textureWidth = 64;
		textureHeight = 64;

		Headwear = new ModelRenderer(this, 0, 21);
		Headwear.addBox(-4F, -8F, -4F, 7, 8, 7);
		Headwear.setRotationPoint(1F, -28F, 1F);
		Headwear.setTextureSize(64, 64);
		setRotation(Headwear, 0F, 0F, 0F);
		headSet = new ModelRenderer(this, 0, 0);
		headSet.setRotationPoint(0F, 0F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		headSet.addBox(-4F, -8F, -4F, 9, 9, 9);

		RightUpperHorn = new ModelRenderer(this, 43, 0);
		RightUpperHorn.setRotationPoint(0F, 0F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		RightUpperHorn.addBox(-9F, -9F, 0F, 2, 5, 2);
		headSet.addChild(RightUpperHorn);
		LeftUpperHorn = new ModelRenderer(this, 43, 0);
		LeftUpperHorn.setRotationPoint(0F, 0F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		LeftUpperHorn.addBox(8F, -9F, 0F, 2, 5, 2);
		headSet.addChild(LeftUpperHorn);
		LeftLowerHorn = new ModelRenderer(this, 8, 55);
		LeftLowerHorn.setRotationPoint(0F, 0F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		LeftLowerHorn.addBox(5F, -6F, 0F, 4, 2, 2);
		headSet.addChild(LeftLowerHorn);
		RightLowerHorn = new ModelRenderer(this, 8, 55);
		RightLowerHorn.setRotationPoint(0F, 0F, 0F);
		setRotation(headSet, 0F, 0F, 0F);
		RightLowerHorn.addBox(-8F, -6F, 0F, 4, 2, 2);
		headSet.addChild(RightLowerHorn);
		textureWidth = 64;
		textureHeight = 64;

		Front1 = new ModelRenderer(this, 26, 55);
		Front1.addBox(0F, 0F, 0F, 11, 2, 6);
		Front1.setRotationPoint(-5F, -25F, -3.333333F);
		Front1.setTextureSize(64, 64);
		setRotation(Front1, 0.1487144F, 0F, 0F);
		Front2 = new ModelRenderer(this, 30, 55);
		Front2.addBox(0F, 0F, 0F, 10, 2, 6);
		Front2.setRotationPoint(-4.5F, -23.13333F, -3.066667F);
		Front2.setTextureSize(64, 64);
		setRotation(Front2, 0.1487144F, 0F, 0F);
		Front3 = new ModelRenderer(this, 32, 53);
		Front3.addBox(0F, 0F, 0F, 9, 2, 5);
		Front3.setRotationPoint(-4F, -21.46667F, -2.8F);
		Front3.setTextureSize(64, 64);
		setRotation(Front3, 0.1487144F, 0F, 0F);
		Front4 = new ModelRenderer(this, 41, 52);
		Front4.addBox(0F, 0F, 0F, 8, 2, 2);
		Front4.setRotationPoint(-3.5F, -19.9F, -2.6F);
		Front4.setTextureSize(64, 64);
		setRotation(Front4, 0.1487144F, 0F, 0F);
		Front5 = new ModelRenderer(this, 34, 56);
		Front5.addBox(0F, 0F, 0F, 7, 2, 5);
		Front5.setRotationPoint(-3F, -18.06667F, -2.3F);
		Front5.setTextureSize(64, 64);
		setRotation(Front5, 0.1487144F, 0F, 0F);
		Front6 = new ModelRenderer(this, 32, 52);
		Front6.addBox(0F, 0F, 0F, 6, 2, 4);
		Front6.setRotationPoint(-2.466667F, -16.33333F, -2F);
		Front6.setTextureSize(64, 64);
		setRotation(Front6, 0.1487144F, 0F, 0F);
		Front7 = new ModelRenderer(this, 29, 57);
		Front7.addBox(0F, 0F, 0F, 5, 2, 3);
		Front7.setRotationPoint(-2F, -14.46667F, -1.6F);
		Front7.setTextureSize(64, 64);
		setRotation(Front7, 0.1487144F, 0F, 0F);
		Front8 = new ModelRenderer(this, 31, 55);
		Front8.addBox(0F, 0F, 0F, 12, 1, 3);
		Front8.setRotationPoint(-5.5F, -26F, -3.5F);
		Front8.setTextureSize(64, 64);
		setRotation(Front8, 0.1487144F, 0F, 0F);
		Top1 = new ModelRenderer(this, 21, 54);
		Top1.addBox(0F, 0F, 0F, 12, 1, 4);
		Top1.setRotationPoint(-5.5F, -27F, -3.5F);
		Top1.setTextureSize(64, 64);
		setRotation(Top1, 0F, 0F, 0F);
		Top2 = new ModelRenderer(this, 21, 54);
		Top2.addBox(0F, 0F, 0F, 12, 1, 5);
		Top2.setRotationPoint(-5.5F, -27F, 0.1F);
		Top2.setTextureSize(64, 64);
		setRotation(Top2, 0F, 0F, 0F);
		Lateral1 = new ModelRenderer(this, 52, 0);
		Lateral1.addBox(0F, 0F, 0F, 1, 13, 4);
		Lateral1.setRotationPoint(5.5F, -25.5F, -3.4F);
		Lateral1.setTextureSize(64, 64);
		setRotation(Lateral1, 0.1487144F, 0F, 0.2230717F);
		Lateral2 = new ModelRenderer(this, 52, 0);
		Lateral2.addBox(0F, 0F, 0F, 1, 13, 4);
		Lateral2.setRotationPoint(-5.5F, -25.5F, -3.4F);
		Lateral2.setTextureSize(64, 64);
		setRotation(Lateral2, 0.1487144F, 0F, -0.2230717F);
		Lateral3 = new ModelRenderer(this, 52, 0);
		Lateral3.addBox(0F, 0F, 0F, 1, 13, 4);
		Lateral3.setRotationPoint(5.7F, -26.5F, 1F);
		Lateral3.setTextureSize(64, 64);
		setRotation(Lateral3, -0.1487144F, 0F, 0.2230717F);
		Lateral4 = new ModelRenderer(this, 52, 0);
		Lateral4.addBox(0F, 0F, 0F, 1, 13, 4);
		Lateral4.setRotationPoint(-5.6F, -26F, 1F);
		Lateral4.setTextureSize(64, 64);
		setRotation(Lateral4, -0.1487144F, 0F, -0.2230717F);
		Lateral5 = new ModelRenderer(this, 32, 57);
		Lateral5.addBox(0F, 0F, 0F, 3, 1, 3);
		Lateral5.setRotationPoint(0.7F, -13.4F, -1.5F);
		Lateral5.setTextureSize(64, 64);
		setRotation(Lateral5, 0F, 0F, 0F);
		Lateral6 = new ModelRenderer(this, 32, 57);
		Lateral6.addBox(0F, 0F, 0F, 4, 2, 3);
		Lateral6.setRotationPoint(-2.7F, -14.4F, 0.1F);
		Lateral6.setTextureSize(64, 64);
		setRotation(Lateral6, 0F, 0F, 0F);
		Lateral7 = new ModelRenderer(this, 25, 57);
		Lateral7.addBox(0F, 0F, 0F, 4, 1, 3);
		Lateral7.setRotationPoint(-2.7F, -13.4F, -1.5F);
		Lateral7.setTextureSize(64, 64);
		setRotation(Lateral7, 0F, 0F, 0F);
		Lateral8 = new ModelRenderer(this, 32, 57);
		Lateral8.addBox(0F, 0F, 0F, 4, 2, 3);
		Lateral8.setRotationPoint(-0.3F, -14.4F, 0.1F);
		Lateral8.setTextureSize(64, 64);
		setRotation(Lateral8, 0F, 0F, 0F);
		RightArm = new ModelRenderer(this, 52, 0);
		RightArm.addBox(0F, 0F, 0F, 3, 16, 3);
		RightArm.setRotationPoint(-8F, -25F, -1F);
		RightArm.setTextureSize(64, 64);
		setRotation(RightArm, 0.1858931F, 0F, 0F);
		RightArmDown = new ModelRenderer(this, 52, 0);
		RightArmDown.addBox(0F, 12.6F, 6.9F, 3, 16, 3);
		RightArmDown.setRotationPoint(-8F, -25F, -1F);
		RightArmDown.setTextureSize(64, 64);
		setRotation(RightArmDown, -0.2974289F, 0F, 0F);
		LeftArm = new ModelRenderer(this, 52, 0);
		LeftArm.addBox(0F, 0F, 0F, 3, 16, 3);
		LeftArm.setRotationPoint(6F, -25F, -1F);
		LeftArm.setTextureSize(64, 64);
		setRotation(LeftArm, 0.1858931F, 0F, 0F);
		LeftArmDown = new ModelRenderer(this, 52, 0);
		LeftArmDown.addBox(0F, 12.6F, 6.933333F, 3, 16, 3);
		LeftArmDown.setRotationPoint(6F, -25F, -1F);
		LeftArmDown.setTextureSize(64, 64);
		LeftArmDown.mirror = true;
		setRotation(LeftArmDown, -0.2974289F, 0F, 0F);
		RightLeg = new ModelRenderer(this, 52, 0);
		RightLeg.addBox(0F, 0F, 0F, 3, 15, 3);
		RightLeg.setRotationPoint(-4F, -7.2F, -1F);
		RightLeg.setTextureSize(64, 64);
		setRotation(RightLeg, -0.0743572F, 0F, 0F);
		RightLegDown = new ModelRenderer(this, 52, 0);
		RightLegDown.addBox(0F, 14.8F, -2.2F, 3, 16, 3);
		RightLegDown.setRotationPoint(-4F, -7.2F, -1F);
		RightLegDown.setTextureSize(64, 64);
		setRotation(RightLegDown, 0.0743572F, 0F, 0F);
		LeftLeg = new ModelRenderer(this, 52, 0);
		LeftLeg.addBox(0F, 0F, 0F, 3, 15, 3);
		LeftLeg.setRotationPoint(2F, -7.2F, -1F);
		LeftLeg.setTextureSize(64, 64);
		setRotation(LeftLeg, -0.0743572F, 0F, 0F);
		LeftLegDown = new ModelRenderer(this, 52, 0);
		LeftLegDown.addBox(0F, 14.8F, -2.2F, 3, 16, 3);
		LeftLegDown.setRotationPoint(2F, -7.2F, -1F);
		LeftLegDown.setTextureSize(64, 64);
		setRotation(LeftLegDown, 0.0743572F, 0F, 0F);
		UpperWaist = new ModelRenderer(this, 40, 58);
		UpperWaist.addBox(0F, 0F, 0F, 9, 3, 3);
		UpperWaist.setRotationPoint(-4F, -10F, -1F);
		UpperWaist.setTextureSize(64, 64);
		setRotation(UpperWaist, 0F, 0F, 0F);
		Neck = new ModelRenderer(this, 40, 57);
		Neck.addBox(0F, 0F, 0F, 3, 2, 3);
		Neck.setRotationPoint(-1F, -29F, -1F);
		Neck.setTextureSize(64, 64);
		setRotation(Neck, 0F, 0F, 0F);
		LowerWaist = new ModelRenderer(this, 40, 58);
		LowerWaist.addBox(0F, 0F, 0F, 3, 3, 3);
		LowerWaist.setRotationPoint(-1F, -13F, -1F);
		LowerWaist.setTextureSize(64, 64);
		setRotation(LowerWaist, 0F, 0F, 0F);
		Back1 = new ModelRenderer(this, 26, 55);
		Back1.addBox(0F, 0F, 0F, 11, 2, 6);
		Back1.setRotationPoint(-5F, -26F, -1.1F);
		Back1.setTextureSize(64, 64);
		setRotation(Back1, -0.1487144F, 0F, 0F);
		Back2 = new ModelRenderer(this, 30, 55);
		Back2.addBox(0F, 0F, 0F, 10, 2, 6);
		Back2.setRotationPoint(-4.5F, -24.1F, -1.4F);
		Back2.setTextureSize(64, 64);
		setRotation(Back2, -0.1487144F, 0F, 0F);
		Back3 = new ModelRenderer(this, 32, 53);
		Back3.addBox(0F, 0F, 0F, 9, 2, 5);
		Back3.setRotationPoint(-4F, -22F, -0.7F);
		Back3.setTextureSize(64, 64);
		setRotation(Back3, -0.1487144F, 0F, 0F);
		Back4 = new ModelRenderer(this, 41, 52);
		Back4.addBox(0F, 0F, 0F, 8, 2, 2);
		Back4.setRotationPoint(-3.5F, -19.7F, 2F);
		Back4.setTextureSize(64, 64);
		setRotation(Back4, -0.1487144F, 0F, 0F);
		Back5 = new ModelRenderer(this, 34, 56);
		Back5.addBox(0F, 0F, 0F, 7, 2, 5);
		Back5.setRotationPoint(-3F, -18.2F, -1.3F);
		Back5.setTextureSize(64, 64);
		setRotation(Back5, -0.1487144F, 0F, 0F);
		Back6 = new ModelRenderer(this, 32, 52);
		Back6.addBox(0F, 0F, 0F, 6, 2, 4);
		Back6.setRotationPoint(-2.5F, -16.1F, -0.6F);
		Back6.setTextureSize(64, 64);
		setRotation(Back6, -0.1487144F, 0F, 0F);
		Back7 = new ModelRenderer(this, 29, 57);
		Back7.addBox(0F, 0F, 0F, 5, 2, 3);
		Back7.setRotationPoint(-2F, -15F, 0.2F);
		Back7.setTextureSize(64, 64);
		setRotation(Back7, -0.1487144F, 0F, 0F);
		Back8 = new ModelRenderer(this, 31, 55);
		Back8.addBox(0F, 0F, 0F, 12, 1, 3);
		Back8.setRotationPoint(-5.5F, -26.5F, 2.1F);
		Back8.setTextureSize(64, 64);
		setRotation(Back8, -0.1487144F, 0F, 0F);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return null;
	}

	@Override
	public void setRotationAngles(T par7Entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.headSet.showModel = true;
		float speed = 0.2262F;

		RightLeg.rotateAngleX = -0.077f + MathHelper.cos(limbSwing * speed) * limbSwingAmount;
		RightLegDown.rotateAngleX = 0.07f + MathHelper.cos(limbSwing * speed) * limbSwingAmount;
		LeftLeg.rotateAngleX = -0.077f + MathHelper.cos(limbSwing * speed + 3.141593F) * limbSwingAmount;
		LeftLegDown.rotateAngleX = 0.07f + MathHelper.cos(limbSwing * speed + 3.141593F) * limbSwingAmount;

		if (par7Entity.deathTicks > 0 || par7Entity.getScreamTimer() > 0)
		{
			this.LeftArm.rotateAngleZ = -0.5F;
			this.LeftArm.rotateAngleY = -0.5F;
			this.LeftArmDown.rotateAngleZ = -0.5F;
			this.LeftArmDown.rotateAngleY = -0.5F;
			this.LeftArm.rotateAngleX = 0.25F;
			this.LeftArmDown.rotateAngleX = -0.23F;

			this.RightArm.rotateAngleZ = 0.5F;
			this.RightArm.rotateAngleY = 0.5F;
			this.RightArmDown.rotateAngleZ = 0.5F;
			this.RightArmDown.rotateAngleY = 0.5F;
			this.RightArm.rotateAngleX = 0.25F;
			this.RightArmDown.rotateAngleX = -0.23F;

			float var7 = -26.0F;

			this.headSet.rotateAngleX = 0 - 0.5F;
			this.Headwear.rotateAngleX = 0 - 0.5F;

			this.headSet.rotationPointZ = -0.0F;
			this.headSet.rotationPointY = var7 - 3.0F;
			this.Headwear.rotationPointZ = 1.0F;
			this.Headwear.rotationPointY = var7 - 2.0F;

			float var9 = 1.0F;
			this.headSet.rotationPointY -= var9 * 5.0F;
		}
		else
		{
			float var7 = -26.0F;

			this.headSet.rotateAngleY = netHeadYaw / 57.29578F;
			this.headSet.rotateAngleX = headPitch / 57.29578F;
			this.Headwear.rotateAngleY = netHeadYaw / 57.29578F;
			this.Headwear.rotateAngleX = headPitch / 57.29578F;

			this.headSet.rotationPointZ = -0.0F;
			this.headSet.rotationPointY = var7 - 3.0F;
			this.Headwear.rotationPointZ = 1.0F;
			this.Headwear.rotationPointY = var7 - 2.0F;

			if (par7Entity.getAngry())
			{
				float var9 = 1.0F;
				this.headSet.rotationPointY -= var9 * 5.0F;
			}
		}
	}

	public void setLivingAnimations(T entityIn, float limbSwing, float limbSwingAmount, float partialTick)
	{
		// GlStateManager.pushMatrix();
		int i = entityIn.getAttackTimer();
		float speed = 0.2262F;
		float var71 = MathHelper.sin((0 - 1.03F) * (float) Math.PI);
		float var8 = MathHelper.sin((1.0F - (1.0F - 0 - 0.5F) * (1.0F - 0 - 0.5F)) * (float) Math.PI);

		this.LeftArm.rotateAngleZ = -0.08F;
		this.LeftArm.rotateAngleY = -(0.1F - var71 * 0.6F);
		this.LeftArm.rotateAngleZ += MathHelper.cos(partialTick * 0.09F) * 0.050F;

		this.LeftArmDown.rotateAngleZ = -0.08F;
		this.LeftArmDown.rotateAngleY = -(0.1F - var71 * 0.6F);
		this.LeftArmDown.rotateAngleZ += MathHelper.cos(partialTick * 0.09F) * 0.050F;

		this.RightArm.rotateAngleZ = 0.08F;
		this.RightArm.rotateAngleY = -(0.1F - var71 * 0.6F);
		this.RightArm.rotateAngleZ -= MathHelper.cos(partialTick * 0.09F) * 0.050F;

		this.RightArmDown.rotateAngleZ = 0.08F;
		this.RightArmDown.rotateAngleY = -(0.1F - var71 * 0.6F);
		this.RightArmDown.rotateAngleZ -= MathHelper.cos(partialTick * 0.09F) * 0.050F;

		this.LeftArm.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F) + MathHelper.cos(limbSwing * speed) * limbSwingAmount;
		this.LeftArm.rotateAngleX -= var71 * (-1.5F) - var8 * (-0.2F);
		this.LeftArm.rotateAngleX += MathHelper.sin(partialTick * 0.067F) * 0.03F;

		this.LeftArmDown.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F) + MathHelper.cos(limbSwing * speed) * limbSwingAmount;
		this.LeftArmDown.rotateAngleX -= var71 * 3.57F - var8 * (-0.2F);
		this.LeftArmDown.rotateAngleX += MathHelper.sin(partialTick * 0.067F) * 0.03F;

		this.RightArm.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F) + MathHelper.cos(limbSwing * speed + 3.141593F) * limbSwingAmount;
		this.RightArm.rotateAngleX -= var71 * (-1.5F) - var8 * (-0.2F);
		this.RightArm.rotateAngleX += MathHelper.sin(partialTick * 0.067F) * 0.03F;

		this.RightArmDown.rotateAngleX = -(((float) Math.PI - 3.55F) / 2F) + MathHelper.cos(limbSwing * speed + 3.141593F) * limbSwingAmount;
		this.RightArmDown.rotateAngleX -= var71 * 3.57F - var8 * (-0.2F);
		this.RightArmDown.rotateAngleX += MathHelper.sin(partialTick * 0.067F) * 0.03F;

		if (i > 0)
		{
			this.RightArm.rotateAngleX = this.RightArm.rotateAngleX + -2.0F + 2.0F * this.func_78172_a((float) i - partialTick, 10.0F);
			this.RightArmDown.rotateAngleX = this.RightArmDown.rotateAngleX + -2.0F + 2.0F * this.func_78172_a((float) i - partialTick, 10.0F);
			this.LeftArm.rotateAngleX = this.LeftArm.rotateAngleX + -2.0F + 2.0F * this.func_78172_a((float) i - partialTick, 10.0F);
			this.LeftArmDown.rotateAngleX = this.LeftArmDown.rotateAngleX + -2.0F + 2.0F * this.func_78172_a((float) i - partialTick, 10.0F);
		}

		// GlStateManager.popMatrix();
	}

	private float func_78172_a(float par1, float par2)
	{
		return (Math.abs(par1 % par2 - par2 * 0.5F) - par2 * 0.25F) / (par2 * 0.25F);
	}
}
