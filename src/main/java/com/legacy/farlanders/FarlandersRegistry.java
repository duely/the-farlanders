package com.legacy.farlanders;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.registry.FarlandersBlocks;
import com.legacy.farlanders.registry.FarlandersEntityTypes;
import com.legacy.farlanders.registry.FarlandersFeatures;
import com.legacy.farlanders.registry.FarlandersItems;
import com.legacy.farlanders.registry.FarlandersSchedules;
import com.legacy.farlanders.registry.FarlandersStructures;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.brain.schedule.Schedule;
import net.minecraft.item.Item;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = TheFarlandersMod.MODID, bus = Bus.MOD)
public class FarlandersRegistry
{
	@SubscribeEvent
	public static void onRegisterSounds(RegistryEvent.Register<SoundEvent> event)
	{
		FarlandersSounds.init(event.getRegistry());
	}

	@SubscribeEvent
	public static void onRegisterBlocks(RegistryEvent.Register<Block> event)
	{
		FarlandersBlocks.init(event);
	}

	@SubscribeEvent
	public static void onRegisterItems(RegistryEvent.Register<Item> event)
	{
		FarlandersItems.init(event);
	}

	@SubscribeEvent
	public static void onRegisterSchedules(Register<Schedule> event)
	{
		FarlandersSchedules.init(event);
	}

	@SubscribeEvent
	public static void onRegisterEntityTypes(Register<EntityType<?>> event)
	{
		FarlandersEntityTypes.init(event);
		FarlandersEntityTypes.registerSpawnPlacements();
	}

	@SubscribeEvent
	public static void onRegisterStructures(Register<Structure<?>> event)
	{
		FarlandersStructures.init(event);
	}
	
	@SubscribeEvent
	public static void onRegisterFeatures(Register<Feature<?>> event)
	{
		FarlandersFeatures.init(event);
	}

	/*@SubscribeEvent
	public static void onRegisterBiomes(Register<Biome> event)
	{
		FarlandersBiomes.init(event);
	}
	
	@SubscribeEvent
	public static void onRegisterDimensions(RegistryEvent.Register<ModDimension> event)
	{
		FarlandersDimensions.init(event);
	}*/

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(TheFarlandersMod.locate(name));
		registry.register(object);
	}
}