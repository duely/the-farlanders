package com.legacy.farlanders.entity.util;

import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.entity.merchant.villager.AbstractVillagerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.BabyEntitySpawnEvent;

public abstract class BreedableVillagerEntity extends AbstractVillagerEntity
{
	private int inLove;
	private UUID playerInLove;

	public BreedableVillagerEntity(EntityType<? extends AbstractVillagerEntity> type, World worldIn)
	{
		super(type, worldIn);
	}

	@Override
	protected void updateAITasks()
	{
		if (this.getGrowingAge() != 0)
			this.inLove = 0;

		super.updateAITasks();
	}

	@Override
	public void livingTick()
	{
		super.livingTick();

		if (this.getGrowingAge() != 0)
			this.inLove = 0;

		if (this.inLove > 0)
		{
			--this.inLove;

			if (this.inLove % 10 == 0)
			{
				double d0 = this.rand.nextGaussian() * 0.02D;
				double d1 = this.rand.nextGaussian() * 0.02D;
				double d2 = this.rand.nextGaussian() * 0.02D;
				this.world.addParticle(ParticleTypes.HEART, this.getPosXRandom(1.0D), this.getPosYRandom() + 0.5D, this.getPosZRandom(1.0D), d0, d1, d2);
			}
		}

	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("InLove", this.inLove);

		if (this.playerInLove != null)
			compound.putUniqueId("LoveCause", this.playerInLove);
	}

	@Override
	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
		this.inLove = compound.getInt("InLove");
		this.playerInLove = compound.hasUniqueId("LoveCause") ? compound.getUniqueId("LoveCause") : null;
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
			return false;

		this.inLove = 0;
		return super.attackEntityFrom(source, amount);
	}

	public boolean isBreedingItem(ItemStack stack)
	{
		return stack.getItem() == Items.APPLE;
	}

	@Override
	public ActionResultType func_230254_b_(PlayerEntity p_230254_1_, Hand p_230254_2_)
	{
		ItemStack itemstack = p_230254_1_.getHeldItem(p_230254_2_);
		if (this.isBreedingItem(itemstack))
		{
			int i = this.getGrowingAge();
			if (!this.world.isRemote && i == 0 && this.canFallInLove())
			{
				this.consumeItemFromStack(p_230254_1_, itemstack);
				this.setInLove(p_230254_1_);
				return ActionResultType.SUCCESS;
			}

			if (this.isChild())
			{
				this.consumeItemFromStack(p_230254_1_, itemstack);
				this.ageUp((int) ((float) (-i / 20) * 0.1F), true);
				return ActionResultType.func_233537_a_(this.world.isRemote);
			}

			if (this.world.isRemote)
			{
				return ActionResultType.CONSUME;
			}
		}

		return super.func_230254_b_(p_230254_1_, p_230254_2_);
	}

	protected void consumeItemFromStack(PlayerEntity player, ItemStack stack)
	{
		if (!player.abilities.isCreativeMode)
			stack.shrink(1);
	}

	public boolean canFallInLove()
	{
		return this.inLove <= 0;
	}

	public void setInLove(@Nullable PlayerEntity player)
	{
		this.inLove = 600;

		if (player != null)
			this.playerInLove = player.getUniqueID();

		this.world.setEntityState(this, (byte) 18);
	}

	public void setInLove(int ticks)
	{
		this.inLove = ticks;
	}

	public int inLove()
	{
		return this.inLove;
	}

	@Nullable
	public ServerPlayerEntity getLoveCause()
	{
		if (this.playerInLove == null)
		{
			return null;
		}
		else
		{
			PlayerEntity playerentity = this.world.getPlayerByUuid(this.playerInLove);
			return playerentity instanceof ServerPlayerEntity ? (ServerPlayerEntity) playerentity : null;
		}
	}

	public boolean isInLove()
	{
		return this.inLove > 0;
	}

	public void resetInLove()
	{
		this.inLove = 0;
	}

	public boolean canMateWith(BreedableVillagerEntity otherVillager)
	{
		if (otherVillager == this)
			return false;
		else if (otherVillager.getClass() != this.getClass())
			return false;
		else
			return this.isInLove() && otherVillager.isInLove();
	}

	public void func_234177_a_(ServerWorld world, BreedableVillagerEntity villager)
	{
		AgeableEntity ageableentity = this.func_241840_a(world, villager);
		BabyEntitySpawnEvent event = new BabyEntitySpawnEvent(this, villager, ageableentity);
		boolean cancelled = MinecraftForge.EVENT_BUS.post(event);
		ageableentity = event.getChild();

		if (cancelled)
		{
			this.setGrowingAge(6000);
			villager.setGrowingAge(6000);
			this.resetInLove();
			villager.resetInLove();
			return;
		}

		if (ageableentity != null)
		{
			ServerPlayerEntity serverplayerentity = this.getLoveCause();

			if (serverplayerentity == null && villager.getLoveCause() != null)
				serverplayerentity = villager.getLoveCause();

			this.setGrowingAge(6000);
			villager.setGrowingAge(6000);
			this.resetInLove();
			villager.resetInLove();
			ageableentity.setChild(true);
			ageableentity.setLocationAndAngles(this.getPosX(), this.getPosY(), this.getPosZ(), 0.0F, 0.0F);
			world.func_242417_l(ageableentity);
			world.setEntityState(this, (byte) 18);

			if (world.getGameRules().getBoolean(GameRules.DO_MOB_LOOT))
				world.addEntity(new ExperienceOrbEntity(world, this.getPosX(), this.getPosY(), this.getPosZ(), this.getRNG().nextInt(7) + 1));
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleStatusUpdate(byte id)
	{
		if (id == 18)
		{
			for (int i = 0; i < 7; ++i)
			{
				double d0 = this.rand.nextGaussian() * 0.02D;
				double d1 = this.rand.nextGaussian() * 0.02D;
				double d2 = this.rand.nextGaussian() * 0.02D;
				this.world.addParticle(ParticleTypes.HEART, this.getPosXRandom(1.0D), this.getPosYRandom() + 0.5D, this.getPosZRandom(1.0D), d0, d1, d2);
			}
		}
		else
		{
			super.handleStatusUpdate(id);
		}

	}
}