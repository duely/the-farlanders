package com.legacy.farlanders.entity.util;

import java.util.Random;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;
import com.legacy.farlanders.registry.FarlandersItems;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.merchant.villager.VillagerTrades;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MerchantOffer;
import net.minecraft.util.IItemProvider;

public class FarlanderTrades
{
	// emerald for items = give items get emeralds
	public static final Int2ObjectMap<VillagerTrades.ITrade[]> farlanderTrades = func_221238_a(ImmutableMap.of(1, new VillagerTrades.ITrade[]{
			new FarlanderTrades.EmeraldForItemsTrade(Items.CLAY_BALL, 23, 15, 1),
			new FarlanderTrades.EmeraldForItemsTrade(Items.COAL, 15, 10, 2),
			new FarlanderTrades.EmeraldForItemsTrade(Items.REDSTONE, 32, 17, 3),
			new FarlanderTrades.EmeraldForItemsTrade(Items.FLINT, 15, 20, 2),
			new FarlanderTrades.ItemsForEmeraldsTrade(Items.SLIME_BALL, 1, 4, 5), //itemsold emeraldcount itemcount xp ItemsForEmeraldsTrade
			new FarlanderTrades.ItemsForEmeraldsTrade(Items.ARROW, 2, 14, 5)},
			2, new VillagerTrades.ITrade[]{new FarlanderTrades.EmeraldForItemsTrade(Blocks.PUMPKIN, 6, 12, 10), //item itemcount maxueses xp EmeraldForItemsTrade
					new FarlanderTrades.ItemsForEmeraldsTrade(Items.FIRE_CHARGE, 2, 4, 4), 
					new FarlanderTrades.ItemsForEmeraldsTrade(Items.APPLE, 2, 5, 16, 3),
					new FarlanderTrades.EmeraldForItemsTrade(Items.BLAZE_POWDER, 13, 16, 5),
					new FarlanderTrades.EmeraldForItemsTrade(Items.BLAZE_ROD, 7, 16, 5),
					new FarlanderTrades.EmeraldForItemsTrade(Items.NETHER_WART, 10, 12, 4),
					new FarlanderTrades.EmeraldForItemsTrade(Items.GLISTERING_MELON_SLICE, 3, 10, 6)}, 
			3, new VillagerTrades.ITrade[]{new FarlanderTrades.ItemsForEmeraldsTrade(Items.PRISMARINE_SHARD, 5, 16, 10), 
					new FarlanderTrades.ItemsForEmeraldsTrade(Blocks.CARVED_PUMPKIN.asItem(), 1, 1, 10)}, 
			4, new VillagerTrades.ITrade[]{new FarlanderTrades.ItemsForEmeraldsTrade(Items.PAINTING, 3, 1, 12, 15)}, 
			5, new VillagerTrades.ITrade[]{new FarlanderTrades.ItemsForEmeraldsTrade(Items.PRISMARINE_CRYSTALS, 2, 16, 30), 
					new FarlanderTrades.ItemsForEmeraldsTrade(Items.ENDER_PEARL, 3, 4, 30)}));
	
	   public static final Int2ObjectMap<VillagerTrades.ITrade[]> wandererTrades = func_221238_a(ImmutableMap.of(1, new VillagerTrades.ITrade[]{
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.LEATHER_CHESTPLATE, 4, 1, 1, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_CHESTPLATE, 6, 1, 5, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.CHAINMAIL_CHESTPLATE, 5, 1, 5, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(FarlandersItems.nightfall_chestplate, 15, 1, 5, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.LEATHER_BOOTS, 1, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_BOOTS, 2, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.CHAINMAIL_BOOTS, 2, 1, 4, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(FarlandersItems.nightfall_boots, 7, 1, 1, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.LEATHER_HELMET, 1, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_HELMET, 3, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.CHAINMAIL_HELMET, 2, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(FarlandersItems.nightfall_helmet, 9, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.LEATHER_LEGGINGS, 3, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_LEGGINGS, 5, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.CHAINMAIL_LEGGINGS, 7, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(FarlandersItems.nightfall_leggings, 11, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.WOODEN_SWORD, 1, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.STONE_SWORD, 2, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_SWORD, 4, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(FarlandersItems.nightfall_sword, 6, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.WOODEN_AXE, 1, 1, 7, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.STONE_AXE, 2, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_AXE, 4, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.WOODEN_PICKAXE, 2, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.STONE_PICKAXE, 3, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_PICKAXE, 4, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.WOODEN_HOE, 1, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.STONE_HOE, 1, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_HOE, 2, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.WOODEN_SHOVEL, 1, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.STONE_SHOVEL, 2, 1, 8, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.IRON_SHOVEL, 3, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.BOW, 3, 1, 12, 1), 
			   new FarlanderTrades.ItemsForEmeraldsTrade(Items.ARROW, 4, 5, 12, 1)}, 
			   2, new VillagerTrades.ITrade[]{
					   new FarlanderTrades.ItemsForEmeraldsTrade(Items.TROPICAL_FISH_BUCKET, 5, 1, 4, 1), 
					   new FarlanderTrades.ItemsForEmeraldsTrade(Items.PUFFERFISH_BUCKET, 5, 1, 4, 1), 
					   new FarlanderTrades.ItemsForEmeraldsTrade(Items.PACKED_ICE, 3, 1, 6, 1), 
					   new FarlanderTrades.ItemsForEmeraldsTrade(Items.BLUE_ICE, 6, 1, 6, 1), 
					   new FarlanderTrades.ItemsForEmeraldsTrade(Items.GUNPOWDER, 1, 1, 8, 1), 
					   new FarlanderTrades.ItemsForEmeraldsTrade(Items.PODZOL, 3, 3, 6, 1)}));

	   
	private static Int2ObjectMap<VillagerTrades.ITrade[]> func_221238_a(ImmutableMap<Integer, VillagerTrades.ITrade[]> p_221238_0_)
	{
		return new Int2ObjectOpenHashMap<>(p_221238_0_);
	}

	static class ItemsForEmeraldsTrade implements VillagerTrades.ITrade
	{

		private final ItemStack itemSold;

		private final int emeraldCount;

		private final int soldItemCount;

		private final int maxUses;

		private final int givenXP;

		private final float priceMultiplier;

		public ItemsForEmeraldsTrade(Block itemSoldIn, int emeraldCountIn, int soldItemCountIn, int maxUsesIn, int givenXPIn)
		{
			this(new ItemStack(itemSoldIn), emeraldCountIn, soldItemCountIn, maxUsesIn, givenXPIn);
		}

		public ItemsForEmeraldsTrade(Item itemSoldIn, int emeraldCountIn, int soldItemCountIn, int givenXPIn)
		{
			this(new ItemStack(itemSoldIn), emeraldCountIn, soldItemCountIn, 12, givenXPIn);
		}

		public ItemsForEmeraldsTrade(Item itemSoldIn, int emeraldCountIn, int soldItemCountIn, int maxUsesIn, int givenXPIn)
		{
			this(new ItemStack(itemSoldIn), emeraldCountIn, soldItemCountIn, maxUsesIn, givenXPIn);
		}

		public ItemsForEmeraldsTrade(ItemStack itemSoldIn, int emeraldCountIn, int soldItemCountIn, int maxUsesIn, int givenXPIn)
		{
			this(itemSoldIn, emeraldCountIn, soldItemCountIn, maxUsesIn, givenXPIn, 0.05F);
		}

		public ItemsForEmeraldsTrade(ItemStack itemSoldIn, int emeraldCountIn, int soldItemCountIn, int maxUsesIn, int givenXPIn, float priceMultiplierIn)
		{
			this.itemSold = itemSoldIn;
			this.emeraldCount = emeraldCountIn;
			this.soldItemCount = soldItemCountIn;
			this.maxUses = maxUsesIn;
			this.givenXP = givenXPIn;
			this.priceMultiplier = priceMultiplierIn;
		}

		public MerchantOffer getOffer(Entity p_221182_1_, Random p_221182_2_)
		{
			return new MerchantOffer(new ItemStack(FarlandersItems.endumium_crystal, this.emeraldCount), new ItemStack(this.itemSold.getItem(), this.soldItemCount), this.maxUses, this.givenXP, this.priceMultiplier);
		}
	}
	
	static class EmeraldForItemsTrade implements VillagerTrades.ITrade
	{

		private final Item field_221183_a;

		private final int field_221184_b;

		private final int field_221185_c;

		private final int field_221186_d;

		private final float field_221187_e;

		public EmeraldForItemsTrade(IItemProvider itemIn, int itemCountIn, int maxUsesIn, int givenXP)
		{
			this.field_221183_a = itemIn.asItem();
			this.field_221184_b = itemCountIn;
			this.field_221185_c = maxUsesIn;
			this.field_221186_d = givenXP;
			this.field_221187_e = 0.05F;
		}

		public MerchantOffer getOffer(Entity p_221182_1_, Random p_221182_2_)
		{
			ItemStack itemstack = new ItemStack(this.field_221183_a, this.field_221184_b);
			return new MerchantOffer(itemstack, new ItemStack(FarlandersItems.endumium_crystal), this.field_221185_c, this.field_221186_d, this.field_221187_e);
		}
	}
	
	static class ItemsForEmeraldsAndItemsTrade implements VillagerTrades.ITrade
	{

		private final ItemStack field_221200_a;

		private final int field_221201_b;

		private final int field_221202_c;

		private final ItemStack field_221203_d;

		private final int field_221204_e;

		private final int field_221205_f;

		private final int field_221206_g;

		private final float field_221207_h;

		public ItemsForEmeraldsAndItemsTrade(IItemProvider p_i50533_1_, int p_i50533_2_, Item p_i50533_3_, int p_i50533_4_, int p_i50533_5_, int p_i50533_6_)
		{
			this(p_i50533_1_, p_i50533_2_, 1, p_i50533_3_, p_i50533_4_, p_i50533_5_, p_i50533_6_);
		}

		public ItemsForEmeraldsAndItemsTrade(IItemProvider p_i50534_1_, int p_i50534_2_, int p_i50534_3_, Item p_i50534_4_, int p_i50534_5_, int p_i50534_6_, int p_i50534_7_)
		{
			this.field_221200_a = new ItemStack(p_i50534_1_);
			this.field_221201_b = p_i50534_2_;
			this.field_221202_c = p_i50534_3_;
			this.field_221203_d = new ItemStack(p_i50534_4_);
			this.field_221204_e = p_i50534_5_;
			this.field_221205_f = p_i50534_6_;
			this.field_221206_g = p_i50534_7_;
			this.field_221207_h = 0.05F;
		}

		@Nullable
		public MerchantOffer getOffer(Entity p_221182_1_, Random p_221182_2_)
		{
			return new MerchantOffer(new ItemStack(FarlandersItems.endumium_crystal, this.field_221202_c), new ItemStack(this.field_221200_a.getItem(), this.field_221201_b), new ItemStack(this.field_221203_d.getItem(), this.field_221204_e), this.field_221205_f, this.field_221206_g, this.field_221207_h);
		}
	}
}
