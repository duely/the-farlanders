package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.FarlandersConfig;
import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.OpenDoorGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.EndermiteEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.AxeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.SwordItem;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.GroundPathNavigator;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;

public class LooterEntity extends MonsterEntity implements IColoredEyes
{
	public static final DataParameter<Integer> EYE_COLOR = EntityDataManager.<Integer>createKey(LooterEntity.class, DataSerializers.VARINT);

	private boolean hasSword;
	private int particleEffect;

	public LooterEntity(EntityType<? extends LooterEntity> type, World world)
	{
		super(type, world);
		((GroundPathNavigator) this.getNavigator()).setBreakDoors(true);
		this.setPathPriority(PathNodeType.WATER, -1.0F);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 1.60F;
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MonsterEntity.func_234295_eP_().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.5D).createMutableAttribute(Attributes.MAX_HEALTH, 30.0D).createMutableAttribute(Attributes.ATTACK_DAMAGE, 3.0D);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.6D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, FarlanderEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, ElderFarlanderEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, WandererEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, EnderGolemEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, EndermiteEntity.class, false));
	}

	@Override
	protected void registerData()
	{
		super.registerData();

		this.dataManager.register(EYE_COLOR, Integer.valueOf(this.rand.nextInt(6)));
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("EyeColor", this.getEyeColor());
		compound.putBoolean("HasSword", this.getHasSword());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setEyeColor(compound.getInt("EyeColor"));
		this.setHasSword(compound.getBoolean("HasSword"));
	}

	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		if (FarlandersConfig.COMMON.disableLooterStealing.get())
		{
			int rand = worldIn.getRandom().nextInt(7);
			Item randomWeapon = rand == 0 ? Items.IRON_AXE : rand == 1 ? Items.STONE_SWORD : rand == 2 ? Items.STONE_AXE : rand == 3 ? Items.DIAMOND_SWORD : rand == 4 ? Items.GOLDEN_SWORD : rand == 5 ? Items.GOLDEN_AXE : Items.IRON_SWORD;
			this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(randomWeapon));
			this.setHasSword(true);
		}

		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	public void onDeath(DamageSource cause)
	{
		super.onDeath(cause);

		if (!FarlandersConfig.COMMON.disableLooterStealing.get())
			this.entityDropItem(this.getHeldItemMainhand());
	}

	@Override
	public boolean canDespawn(double distanceToClosestPlayer)
	{
		if (!FarlandersConfig.COMMON.disableLooterStealing.get() && !this.getHeldItemMainhand().isEmpty())
			return false;
		else
			return super.canDespawn(distanceToClosestPlayer);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubbleColumn() == true)
		{
			this.attackEntityFrom(DamageSource.DROWN, 1);
		}

		if (this.getHeldItemMainhand() != ItemStack.EMPTY)
		{
			if (this.getEyeColor() == 0)
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.PORTAL, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
			}
			else if (this.getEyeColor() == 1)
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.SMOKE, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
			}
			else if (this.getEyeColor() == 2)
			{
				this.world.addParticle(ParticleTypes.FLAME, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
			}
			else if (this.getEyeColor() == 3)
			{
				for (int k = 0; k < 2; k++)
					this.world.addParticle(ParticleTypes.MYCELIUM, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
			}
			else if (this.getEyeColor() == 4)
			{
				particleEffect++;
				if (particleEffect == 3)
				{
					this.world.addParticle(ParticleTypes.HAPPY_VILLAGER, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
					particleEffect = 0;
				}
			}
			else if (this.getEyeColor() == 5)
			{
				particleEffect++;
				if (particleEffect == 1)
				{
					this.world.addParticle(ParticleTypes.WITCH, getPosX() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (getPosY() + rand.nextDouble() * (double) this.getHeight()) - 0.25D, getPosZ() + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (rand.nextDouble() - 0.5D) * 2D, 0.5d + rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
					particleEffect = 0;
				}
			}
		}
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (source.getImmediateSource() instanceof PlayerEntity && !source.isCreativePlayer() && !FarlandersConfig.COMMON.disableLooterStealing.get() && amount < this.getHealth() && !this.world.isRemote)
		{
			PlayerEntity player = (PlayerEntity) source.getImmediateSource();

			if (this.getHasSword() == false)
			{
				if (player.inventory.getCurrentItem() != null)
				{
					if (player.inventory.getCurrentItem().getItem() instanceof SwordItem || player.inventory.getCurrentItem().getItem() instanceof AxeItem)
					{
						ItemStack copy = player.getHeldItemMainhand().copy();
						this.setItemStackToSlot(EquipmentSlotType.MAINHAND, copy);
						player.setItemStackToSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY);
						this.setHasSword(true);
						this.world.playSound(player, this.getPosition(), SoundEvents.ENTITY_CHICKEN_EGG, SoundCategory.HOSTILE, 1.0F, 1.0F);
						this.world.playSound(player, this.getPosition(), SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.HOSTILE, 1.0F, 1.0F);
						ItemStack var2 = this.getHeldItem(Hand.MAIN_HAND);
						double var3 = 1.5D;
						try
						{
							var3 += ((SwordItem) var2.getItem()).getAttackDamage();
						}
						catch (Exception e)
						{
							var3 = 3.0F;
						}
						finally
						{
							this.getAttribute(Attributes.ATTACK_DAMAGE).setBaseValue(var3);
						}

						super.setDropChance(EquipmentSlotType.MAINHAND, 0.0F);
					}
				}
			}
		}
		return super.attackEntityFrom(source, amount);
	}

	public boolean getHasSword()
	{
		return this.hasSword;
	}

	public void setHasSword(boolean hasSword)
	{
		this.hasSword = hasSword;
	}

	public void setEyeColor(int colorID)
	{
		this.dataManager.set(EYE_COLOR, colorID);
	}

	public int getEyeColor()
	{
		return this.dataManager.get(EYE_COLOR);
	}

	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_FARLANDER_IDLE;
	}

	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FarlandersSounds.ENTITY_FARLANDER_HURT;
	}

	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_FARLANDER_DEATH;
	}
}