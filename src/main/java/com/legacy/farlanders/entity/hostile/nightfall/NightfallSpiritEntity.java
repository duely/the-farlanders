package com.legacy.farlanders.entity.hostile.nightfall;

import net.minecraft.entity.EntityType;
import net.minecraft.world.World;

public class NightfallSpiritEntity extends EndSpiritEntity
{

	public NightfallSpiritEntity(EntityType<? extends NightfallSpiritEntity> type, World world)
	{
		super(type, world);
	}
}
