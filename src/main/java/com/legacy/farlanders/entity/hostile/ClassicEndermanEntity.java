package com.legacy.farlanders.entity.hostile;

import com.legacy.farlanders.entity.util.BaseEndermanEntity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.world.World;

public class ClassicEndermanEntity extends BaseEndermanEntity
{

	public ClassicEndermanEntity(EntityType<? extends ClassicEndermanEntity> type, World world)
	{
		super(type, world);
	}

	protected void registerExtraGoals()
	{
		this.goalSelector.addGoal(1, new BaseEndermanEntity.StareGoal(this));
		this.goalSelector.addGoal(10, new BaseEndermanEntity.PlaceBlockGoal(this));
		this.goalSelector.addGoal(11, new BaseEndermanEntity.TakeBlockGoal(this));
		this.targetSelector.addGoal(2, new ClassicEndermanEntity.FindPlayerGoal(this));
	}

	@Override
	protected void spawnParticles()
	{
		this.world.addParticle(ParticleTypes.LARGE_SMOKE, this.getPosX() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.getPosY() + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.getPosZ() + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0); // largesmoke
	}
	
	@Override
	protected void onPlayerSeen(PlayerEntity player)
	{
		player.addPotionEffect(new EffectInstance(Effects.NAUSEA, 8 * 41));
	}
}