package com.legacy.farlanders.entity.hostile.boss.summon;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.entity.hostile.boss.EnderColossusEntity;
import com.legacy.farlanders.entity.util.MiniDragonPartEntity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.entity.projectile.SmallFireballEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityPredicates;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.BossInfo;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerBossInfo;

public class MiniDragonEntity extends MobEntity implements IMob
{
	private final ServerBossInfo bossInfo = (ServerBossInfo) (new ServerBossInfo(this.getDisplayName(), BossInfo.Color.GREEN, BossInfo.Overlay.PROGRESS)).setDarkenSky(false);

	public double targetX;
	public double targetY;
	public double targetZ;

	public double[][] ringBuffer = new double[64][3];
	public int ringBufferIndex = -1;

	public MiniDragonPartEntity[] dragonPartArray;
	public MiniDragonPartEntity dragonPartHead;
	public MiniDragonPartEntity dragonPartBody;
	public MiniDragonPartEntity dragonPartTail1;
	public MiniDragonPartEntity dragonPartTail2;
	public MiniDragonPartEntity dragonPartTail3;
	public MiniDragonPartEntity dragonPartWing1;
	public MiniDragonPartEntity dragonPartWing2;

	public float prevAnimTime;
	public float animTime;
	public boolean forceNewTarget;
	public boolean slowed;
	private Entity target;
	public int deathTicks;
	private boolean shotFireball;
	private float randomYawVelocity; //TODO
	public double posX = this.getPosX();
	public double posY = this.getPosY();
	public double posZ = this.getPosZ();

	public MiniDragonEntity(EntityType<? extends MiniDragonEntity> type, World world)
	{
		super(type, world);
		this.shotFireball = false;
		this.dragonPartArray = new MiniDragonPartEntity[] { this.dragonPartHead = new MiniDragonPartEntity(this, "head", 6.0F, 6.0F), this.dragonPartBody = new MiniDragonPartEntity(this, "body", 8.0F, 8.0F), this.dragonPartTail1 = new MiniDragonPartEntity(this, "tail", 4.0F, 4.0F), this.dragonPartTail2 = new MiniDragonPartEntity(this, "tail", 4.0F, 4.0F), this.dragonPartTail3 = new MiniDragonPartEntity(this, "tail", 4.0F, 4.0F), this.dragonPartWing1 = new MiniDragonPartEntity(this, "wing", 4.0F, 4.0F), this.dragonPartWing2 = new MiniDragonPartEntity(this, "wing", 4.0F, 4.0F) };
		this.setHealth(this.getMaxHealth());
		this.noClip = false;
		this.targetY = 100.0D;
		this.ignoreFrustumCheck = true;
	}

	/*protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(50.0D);
	}*/

	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);

		if (this.hasCustomName())
		{
			this.bossInfo.setName(this.getDisplayName());
		}
	}

	public void setCustomName(@Nullable ITextComponent name)
	{
		super.setCustomName(name);
		this.bossInfo.setName(this.getDisplayName());
	}

	public void addTrackingPlayer(ServerPlayerEntity player)
	{
		super.addTrackingPlayer(player);
		this.bossInfo.addPlayer(player);
	}

	public void removeTrackingPlayer(ServerPlayerEntity player)
	{
		super.removeTrackingPlayer(player);
		this.bossInfo.removePlayer(player);
	}

	public double[] getMovementOffsets(int p_70974_1_, float p_70974_2_)
	{
		if (this.getHealth() <= 0.0F)
		{
			p_70974_2_ = 0.0F;
		}

		p_70974_2_ = 1.0F - p_70974_2_;
		int j = this.ringBufferIndex - p_70974_1_ * 1 & 63;
		int k = this.ringBufferIndex - p_70974_1_ * 1 - 1 & 63;
		double[] adouble = new double[3];
		double d0 = this.ringBuffer[j][0];
		double d1 = MathHelper.wrapDegrees(this.ringBuffer[k][0] - d0);
		adouble[0] = d0 + d1 * (double) p_70974_2_;
		d0 = this.ringBuffer[j][1];
		d1 = this.ringBuffer[k][1] - d0;
		adouble[1] = d0 + d1 * (double) p_70974_2_;
		adouble[2] = this.ringBuffer[j][2] + (this.ringBuffer[k][2] - this.ringBuffer[j][2]) * (double) p_70974_2_;
		return adouble;
	}

	@Override
	public void livingTick()
	{
		float f;
		float f1;

		this.remove();

		if (!this.isAIDisabled())
		{
			this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());

			if (this.world.isRemote)
			{
				f = MathHelper.cos(this.animTime * (float) Math.PI * 2.0F);
				f1 = MathHelper.cos(this.prevAnimTime * (float) Math.PI * 2.0F);

				if (f1 <= -0.3F && f >= -0.3F)
				{
					this.world.playSound(this.posX, this.posY, this.posZ, SoundEvents.ENTITY_ENDER_DRAGON_FLAP, SoundCategory.HOSTILE, 3.0F, 1.0F + this.rand.nextFloat() * 0.3F, false);
				}
			}

			this.prevAnimTime = this.animTime;

			if (this.getHealth() <= 0.0F)
			{
				f = (this.rand.nextFloat() - 0.5F) * 8.0F;
				f1 = (this.rand.nextFloat() - 0.5F) * 4.0F;
				for (int i = 0; i < 3; i++)
				{
					this.world.addParticle(ParticleTypes.LAVA, this.posX + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), this.posY + this.rand.nextDouble() * (double) this.getHeight() - 0.25D, this.posZ + (this.rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
				}
			}
			else
			{
				for (int i = 0; i < 10; i++)
					world.addParticle(ParticleTypes.SMOKE, posX + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (posY + rand.nextDouble() * (double) this.getHeight()) - 0.25D, posZ + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
				world.addParticle(ParticleTypes.FLAME, posX + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), (posY + rand.nextDouble() * (double) this.getHeight()) - 0.25D, posZ + (rand.nextDouble() - 0.5D) * (double) this.getWidth(), 0, 0, 0);
				Vector3d vec3d4 = this.getMotion();
				float f12 = 0.2F / (MathHelper.sqrt(horizontalMag(vec3d4)) * 10.0F + 1.0F);
				f12 = f12 * (float) Math.pow(2.0D, vec3d4.y);

				if (this.slowed)
				{
					this.animTime += f12 * 0.5F;
				}
				else
				{
					this.animTime += f12;
				}

				this.rotationYaw = MathHelper.wrapDegrees(this.rotationYaw);
				if (this.isAIDisabled())
				{
					this.animTime = 0.5F;
				}
				else
				{
					if (this.ringBufferIndex < 0)
					{
						for (int i = 0; i < this.ringBuffer.length; ++i)
						{
							this.ringBuffer[i][0] = (double) this.rotationYaw;
							this.ringBuffer[i][1] = this.posY;
						}
					}

					if (++this.ringBufferIndex == this.ringBuffer.length)
					{
						this.ringBufferIndex = 0;
					}

					this.ringBuffer[this.ringBufferIndex][0] = (double) this.rotationYaw;
					this.ringBuffer[this.ringBufferIndex][1] = this.posY;
					if (this.world.isRemote)
					{
						if (this.newPosRotationIncrements > 0)
						{
							double d7 = this.posX + (this.interpTargetX - this.posX) / (double) this.newPosRotationIncrements;
							double d0 = this.posY + (this.interpTargetY - this.posY) / (double) this.newPosRotationIncrements;
							double d1 = this.posZ + (this.interpTargetZ - this.posZ) / (double) this.newPosRotationIncrements;
							double d2 = MathHelper.wrapDegrees(this.interpTargetYaw - (double) this.rotationYaw);
							this.rotationYaw = (float) ((double) this.rotationYaw + d2 / (double) this.newPosRotationIncrements);
							this.rotationPitch = (float) ((double) this.rotationPitch + (this.interpTargetPitch - (double) this.rotationPitch) / (double) this.newPosRotationIncrements);
							--this.newPosRotationIncrements;
							this.setPosition(d7, d0, d1);
							this.setRotation(this.rotationYaw, this.rotationPitch);
						}

					}
					else
					{
						Vector3d vec3d = new Vector3d(this.targetX, this.targetY, this.targetZ);

						// if (this.target != null)
						{
							double d8 = vec3d.x - this.posX;
							double d9 = vec3d.y - this.posY;
							double d10 = vec3d.z - this.posZ;
							double d3 = d8 * d8 + d9 * d9 + d10 * d10;
							float f5 = 10;
							double d4 = (double) MathHelper.sqrt(d8 * d8 + d10 * d10);
							if (d4 > 0.0D)
							{
								d9 = MathHelper.clamp(d9 / d4, (double) (-f5), (double) f5);
							}

							this.setMotion(this.getMotion().add(0.0D, d9 * 0.01D, 0.0D));
							this.rotationYaw = MathHelper.wrapDegrees(this.rotationYaw);
							double d5 = MathHelper.clamp(MathHelper.wrapDegrees(180.0D - MathHelper.atan2(d8, d10) * (double) (180F / (float) Math.PI) - (double) this.rotationYaw), -50.0D, 50.0D);
							Vector3d vec3d1 = vec3d.subtract(this.posX, this.posY, this.posZ).normalize();
							Vector3d vec3d2 = (new Vector3d((double) MathHelper.sin(this.rotationYaw * ((float) Math.PI / 180F)), this.getMotion().y, (double) (-MathHelper.cos(this.rotationYaw * ((float) Math.PI / 180F))))).normalize();
							float f8 = Math.max(((float) vec3d2.dotProduct(vec3d1) + 0.5F) / 1.5F, 0.0F);
							this.randomYawVelocity *= 0.8F;
							this.randomYawVelocity = (float) ((double) this.randomYawVelocity + d5);
							this.rotationYaw += this.randomYawVelocity * 0.1F;
							float f9 = (float) (2.0D / (d3 + 1.0D));
							this.moveRelative(0.06F * (f8 * f9 + (1.0F - f9)), new Vector3d(0.0D, 0.0D, -1.0D));
							if (this.slowed)
							{
								this.move(MoverType.SELF, this.getMotion().scale((double) 0.8F));
							}
							else
							{
								this.move(MoverType.SELF, this.getMotion());
							}

							Vector3d vec3d3 = this.getMotion().normalize();
							double d6 = 0.8D + 0.15D * (vec3d3.dotProduct(vec3d2) + 1.0D) / 2.0D;
							this.setMotion(this.getMotion().mul(d6, (double) 0.91F, d6));
						}

						if (this.target != null)
						{
							this.targetX = this.target.getPosX();
							this.targetZ = this.target.getPosZ();
							double d3 = this.targetX - this.posX;
							double d5 = this.targetZ - this.posZ;
							double d7 = Math.sqrt(d3 * d3 + d5 * d5);
							double d8 = 0.4000000059604645D + d7 / 80.0D - 1.0D;

							if (d8 > 10.0D)
							{
								d8 = 10.0D;
							}

							this.targetY = this.target.getBoundingBox().minY + d8;
						}
						else
						{
							this.targetX += this.rand.nextGaussian() * 2.0D;
							this.targetZ += this.rand.nextGaussian() * 2.0D;
						}

						double d10 = this.targetX - this.posX;
						double d0 = this.targetY - this.posY;
						double d1 = this.targetZ - this.posZ;
						double d2 = d10 * d10 + d0 * d0 + d1 * d1;

						if (this.forceNewTarget || d2 < 100.0D || d2 > 22500.0D || this.collidedHorizontally || this.collidedVertically)
						{
							this.setNewTarget();
						}
					}

					this.renderYawOffset = this.rotationYaw;
					Vector3d[] avec3d = new Vector3d[this.dragonPartArray.length];

					for (int j = 0; j < this.dragonPartArray.length; ++j)
					{
						avec3d[j] = new Vector3d(this.dragonPartArray[j].getPosX(), this.dragonPartArray[j].getPosY(), this.dragonPartArray[j].getPosZ());
					}

					float f15 = (float) (this.getMovementOffsets(5, 1.0F)[1] - this.getMovementOffsets(10, 1.0F)[1]) * 10.0F * ((float) Math.PI / 180F);
					float f16 = MathHelper.cos(f15);
					float f21 = MathHelper.sin(f15);
					float f17 = this.rotationYaw * ((float) Math.PI / 180F);
					float f3 = MathHelper.sin(f17);
					float f18 = MathHelper.cos(f17);
					this.dragonPartBody.tick();
					this.dragonPartBody.setLocationAndAngles(this.posX + (double) (f3 * 0.5F), this.posY, this.posZ - (double) (f18 * 0.5F), 0.0F, 0.0F);
					this.dragonPartWing1.tick();
					this.dragonPartWing1.setLocationAndAngles(this.posX + (double) (f18 * 4.5F), this.posY + 2.0D, this.posZ + (double) (f3 * 4.5F), 0.0F, 0.0F);
					this.dragonPartWing2.tick();
					this.dragonPartWing2.setLocationAndAngles(this.posX - (double) (f18 * 4.5F), this.posY + 2.0D, this.posZ - (double) (f3 * 4.5F), 0.0F, 0.0F);
					if (!this.world.isRemote && this.hurtTime == 0)
					{
						this.collideWithEntities(this.world.getEntitiesInAABBexcluding(this, this.dragonPartWing1.getBoundingBox().grow(4.0D, 2.0D, 4.0D).offset(0.0D, -2.0D, 0.0D), EntityPredicates.CAN_AI_TARGET));
						this.collideWithEntities(this.world.getEntitiesInAABBexcluding(this, this.dragonPartWing2.getBoundingBox().grow(4.0D, 2.0D, 4.0D).offset(0.0D, -2.0D, 0.0D), EntityPredicates.CAN_AI_TARGET));
						this.attackEntitiesInList(this.world.getEntitiesInAABBexcluding(this, this.dragonPartHead.getBoundingBox().grow(1.0D), EntityPredicates.CAN_AI_TARGET));
					}

					double[] adouble = this.getMovementOffsets(5, 1.0F);
					float f19 = MathHelper.sin(this.rotationYaw * ((float) Math.PI / 180F) - this.randomYawVelocity * 0.01F);
					float f4 = MathHelper.cos(this.rotationYaw * ((float) Math.PI / 180F) - this.randomYawVelocity * 0.01F);
					this.dragonPartHead.tick();
					float f20 = 0;
					this.dragonPartHead.setLocationAndAngles(this.posX + (double) (f19 * 6.5F * f16), this.posY + (double) f20 + (double) (f21 * 6.5F), this.posZ - (double) (f4 * 6.5F * f16), 0.0F, 0.0F);

					for (int k = 0; k < 3; ++k)
					{
						MiniDragonPartEntity enderdragonpartentity = null;
						if (k == 0)
						{
							enderdragonpartentity = this.dragonPartTail1;
						}

						if (k == 1)
						{
							enderdragonpartentity = this.dragonPartTail2;
						}

						if (k == 2)
						{
							enderdragonpartentity = this.dragonPartTail3;
						}

						double[] adouble1 = this.getMovementOffsets(12 + k * 2, 1.0F);
						float f6 = MathHelper.sin(f21);
						float f22 = MathHelper.cos(f21);
						float f23 = (float) (k + 1) * 2.0F;
						enderdragonpartentity.tick();
						enderdragonpartentity.setLocationAndAngles(this.posX - (double) ((f3 * 1.5F + f6 * f23) * f16), this.posY + (adouble1[1] - adouble[1]) - (double) ((f23 + 1.5F) * f21) + 1.5D, this.posZ + (double) ((f18 * 1.5F + f22 * f23) * f16), 0.0F, 0.0F);
					}

					if (!this.world.isRemote)
					{
						this.slowed = this.destroyBlocksInAABB(this.dragonPartHead.getBoundingBox()) | this.destroyBlocksInAABB(this.dragonPartBody.getBoundingBox());
					}

					for (int l = 0; l < this.dragonPartArray.length; ++l)
					{
						this.dragonPartArray[l].prevPosX = avec3d[l].x;
						this.dragonPartArray[l].prevPosY = avec3d[l].y;
						this.dragonPartArray[l].prevPosZ = avec3d[l].z;
					}

				}
			}

			if (this.target != null)
			{
				if (this.target instanceof PlayerEntity)
				{
					if (this.target.getDistanceSq(this) < 256.0D && shotFireball == false)
					{
						double d0 = target.getPosX() - this.posX;
						double d1 = target.getBoundingBox().minY + (double) (target.getHeight() / 2.0F) - (this.posY + (double) (this.getHeight() / 2.0F));
						double d2 = target.getPosZ() - this.posZ;
						double d5 = this.target.getPosX() - this.posX;
						double d6 = this.target.getBoundingBox().minY + (double) (this.target.getHeight() / 2.0F) - (this.posY + (double) (this.getHeight() / 2.0F));
						double d7 = this.target.getPosZ() - this.posZ;

						if (rand.nextInt(15) == 0)
						{
							for (int i = 0; i < 3; i++)
							{
								SmallFireballEntity entitysmallfireball = new SmallFireballEntity(this.world, this, d0 + this.rand.nextGaussian() * 0.6D, d1, d2 + this.rand.nextGaussian() * 0.6D);
								entitysmallfireball.setPosition(entitysmallfireball.getPosX(), this.posY + (double) (this.getHeight() / 2.0F) - 0.5D, entitysmallfireball.getPosZ());;
								this.world.addEntity(entitysmallfireball);
								shotFireball = true;
								world.playEvent((PlayerEntity) null, 1017, new BlockPos(this.getPositionVec()), 0);
							}
						}

						else if (rand.nextInt(10) == 0)
						{
							world.playEvent((PlayerEntity) null, 1017, new BlockPos(this.getPositionVec()), 0);
							FireballEntity entitylargefireball = new FireballEntity(this.world, this, d5, d6, d7);
							entitylargefireball.setPosition(entitylargefireball.getPosX(), this.posY + (double) (this.getHeight() / 2.0F) - 0.5D, entitylargefireball.getPosZ());;

							this.world.addEntity(entitylargefireball);
							shotFireball = true;
						}

						else if (rand.nextInt(5) == 0)
						{
							SmallFireballEntity entitysmallfireball = new SmallFireballEntity(this.world, this, d0 + this.rand.nextGaussian() * 0.6D, d1, d2 + this.rand.nextGaussian() * 0.6D);
							entitysmallfireball.setPosition(entitysmallfireball.getPosX(), this.posY + (double) (this.getHeight() / 2.0F) - 0.5D, entitysmallfireball.getPosZ());;

							this.world.addEntity(entitysmallfireball);
							shotFireball = true;
							world.playEvent((PlayerEntity) null, 1017, new BlockPos(this.getPositionVec()), 0);

						}
					}
					else if (this.target.getDistanceSq(this) > 256.0D)
						shotFireball = false;
				}
			}
		}
	}

	private void collideWithEntities(List<?> p_70970_1_)
	{
		double d0 = (this.getBoundingBox().minX + this.getBoundingBox().maxX) / 2.0D;
		double d1 = (this.getBoundingBox().minZ + this.getBoundingBox().maxZ) / 2.0D;
		Iterator<?> iterator = p_70970_1_.iterator();

		while (iterator.hasNext())
		{
			Entity entity = (Entity) iterator.next();

			if (entity instanceof LivingEntity && !(entity instanceof MiniDragonEntity) && !(entity instanceof EnderColossusEntity) && !(entity instanceof EnderColossusShadowEntity))
			{
				double d2 = entity.getPosX() - d0;
				double d3 = entity.getPosZ() - d1;
				double d4 = d2 * d2 + d3 * d3;
				entity.addVelocity(d2 / d4 * 1.1D, 0.20000000298023224D, d3 / d4 * 0.7D);
			}
		}
	}

	private void attackEntitiesInList(List<?> p_70971_1_)
	{
		for (int i = 0; i < p_70971_1_.size(); ++i)
		{
			Entity entity = (Entity) p_70971_1_.get(i);

			if (entity instanceof LivingEntity && !(entity instanceof MiniDragonEntity) && !(entity instanceof EnderColossusEntity) && !(entity instanceof EnderColossusShadowEntity))
			{
				entity.attackEntityFrom(DamageSource.causeMobDamage(this), 1.0F);
			}
		}
	}

	private void setNewTarget()
	{
		this.forceNewTarget = false;

		if (this.rand.nextInt(2) == 0 && !this.world.getPlayers().isEmpty())
		{
			this.target = (Entity) this.world.getPlayers().get(this.rand.nextInt(this.world.getPlayers().size()));
		}
		else
		{
			boolean flag = false;

			do
			{
				this.targetX = 0.0D;
				this.targetY = (double) (70.0F + this.rand.nextFloat() * 50.0F);
				this.targetZ = 0.0D;
				this.targetX += (double) (this.rand.nextFloat() * 120.0F - 60.0F);
				this.targetZ += (double) (this.rand.nextFloat() * 120.0F - 60.0F);
				double d0 = this.posX - this.targetX;
				double d1 = this.posY - this.targetY;
				double d2 = this.posZ - this.targetZ;
				flag = d0 * d0 + d1 * d1 + d2 * d2 > 100.0D;
			}
			while (!flag);

			this.target = null;
		}
	}

	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		return super.attackEntityFrom(source, amount);
	}

	public boolean func_213403_a(MiniDragonPartEntity p_70965_1_, DamageSource p_70965_2_, float p_70965_3_)
	{
		if (p_70965_1_ != this.dragonPartHead)
		{
			p_70965_3_ = p_70965_3_ / 4.0F + 1.0F;
		}

		float f1 = this.rotationYaw * (float) Math.PI / 180.0F;
		float f2 = MathHelper.sin(f1);
		float f3 = MathHelper.cos(f1);
		this.targetX = this.posX + (double) (f2 * 5.0F) + (double) ((this.rand.nextFloat() - 0.5F) * 2.0F);
		this.targetY = this.posY + (double) (this.rand.nextFloat() * 3.0F) + 1.0D;
		this.targetZ = this.posZ - (double) (f3 * 5.0F) + (double) ((this.rand.nextFloat() - 0.5F) * 2.0F);
		this.target = null;

		if (p_70965_2_.getTrueSource() instanceof PlayerEntity || p_70965_2_.isExplosion())
		{
			this.attackEntityFrom(p_70965_2_, p_70965_3_);
		}

		return true;
	}

	/*public boolean func_213403_a(EnderDragonPartEntity p_213403_1_, DamageSource p_213403_2_, float p_213403_3_) {
	      p_213403_3_ = this.phaseManager.getCurrentPhase().func_221113_a(p_213403_2_, p_213403_3_);
	      if (p_213403_1_ != this.dragonPartHead) {
	         p_213403_3_ = p_213403_3_ / 4.0F + Math.min(p_213403_3_, 1.0F);
	      }
	
	      if (p_213403_3_ < 0.01F) {
	         return false;
	      } else {
	         if (p_213403_2_.getTrueSource() instanceof PlayerEntity || p_213403_2_.isExplosion()) {
	            float f = this.getHealth();
	            this.attackDragonFrom(p_213403_2_, p_213403_3_);
	            if (this.getHealth() <= 0.0F && !this.phaseManager.getCurrentPhase().getIsStationary()) {
	               this.setHealth(1.0F);
	               this.phaseManager.setPhase(PhaseType.DYING);
	            }
	
	            if (this.phaseManager.getCurrentPhase().getIsStationary()) {
	               this.sittingDamageReceived = (int)((float)this.sittingDamageReceived + (f - this.getHealth()));
	               if ((float)this.sittingDamageReceived > 0.25F * this.getMaxHealth()) {
	                  this.sittingDamageReceived = 0;
	                  this.phaseManager.setPhase(PhaseType.TAKEOFF);
	               }
	            }
	         }
	
	         return true;
	      }
	   }*/

	protected boolean attackDragonFrom(DamageSource source, float amount)
	{
		return super.attackEntityFrom(source, amount);
	}

	private boolean destroyBlocksInAABB(AxisAlignedBB p_70972_1_)
	{
		return false;
	}

	protected void onDeathUpdate()
	{
		++this.deathTicks;
		if (this.deathTicks >= 100 && this.deathTicks <= 150)
		{
			float f = (this.rand.nextFloat() - 0.5F) * 8.0F;
			float f1 = (this.rand.nextFloat() - 0.5F) * 4.0F;
			float f2 = (this.rand.nextFloat() - 0.5F) * 8.0F;
			this.world.addParticle(ParticleTypes.EXPLOSION_EMITTER, this.posX + (double) f, this.posY + 2.0D + (double) f1, this.posZ + (double) f2, 0.0D, 0.0D, 0.0D);
		}

		// int i = 500;

		if (!this.world.isRemote)
		{
			if (this.deathTicks == 1)
			{
				this.playSound(SoundEvents.ENTITY_ENDER_DRAGON_DEATH, 5.0F, 1.2F);
			}
		}

		this.move(MoverType.SELF, new Vector3d(0.0D, (double) 0.1F, 0.0D));
		this.rotationYaw += 20.0F;
		this.renderYawOffset = this.rotationYaw;
		if (this.deathTicks == 150 && !this.world.isRemote)
		{
			this.remove();
		}
	}

	protected boolean canDespawn()
	{
		return false;
	}

	public Entity[] getParts()
	{
		return this.dragonPartArray;
	}

	public boolean canBeCollidedWith()
	{
		return true;
	}

	public World func_82194_d()
	{
		return this.world;
	}

	protected SoundEvent getAmbientSound()
	{
		return SoundEvents.ENTITY_ENDER_DRAGON_AMBIENT;
	}

	protected SoundEvent getHurtSound(DamageSource source)
	{
		return SoundEvents.ENTITY_ENDER_DRAGON_HURT;
	}

	protected float getSoundVolume()
	{
		return 3.0F;
	}

	protected float getSoundPitch()
	{
		return 1.7f;
	}

	public boolean isNonBoss()
	{
		return false;
	}

	public void onKillCommand()
	{
		this.remove();
	}
}