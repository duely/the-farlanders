package com.legacy.farlanders.entity.ai.tasks;

import com.google.common.collect.ImmutableMap;
import com.legacy.farlanders.entity.FarlanderEntity;

import net.minecraft.entity.ai.brain.Brain;
import net.minecraft.entity.ai.brain.memory.MemoryModuleStatus;
import net.minecraft.entity.ai.brain.memory.MemoryModuleType;
import net.minecraft.entity.ai.brain.memory.WalkTarget;
import net.minecraft.entity.ai.brain.task.Task;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.EntityPosWrapper;
import net.minecraft.world.server.ServerWorld;

public class FarlanderTradeTask extends Task<FarlanderEntity>
{
	private final float speed;

	public FarlanderTradeTask(float speedIn)
	{
		super(ImmutableMap.of(MemoryModuleType.WALK_TARGET, MemoryModuleStatus.REGISTERED, MemoryModuleType.LOOK_TARGET, MemoryModuleStatus.REGISTERED), Integer.MAX_VALUE);
		this.speed = speedIn;
	}

	protected boolean shouldExecute(ServerWorld worldIn, FarlanderEntity owner)
	{
		PlayerEntity playerentity = owner.getCustomer();
		return owner.isAlive() && playerentity != null && !owner.isInWater() && !owner.velocityChanged && owner.getDistanceSq(playerentity) <= 16.0D && playerentity.openContainer != null;
	}

	protected boolean shouldContinueExecuting(ServerWorld worldIn, FarlanderEntity entityIn, long gameTimeIn)
	{
		return this.shouldExecute(worldIn, entityIn);
	}

	protected void startExecuting(ServerWorld worldIn, FarlanderEntity entityIn, long gameTimeIn)
	{
		this.walkAndLookCustomer(entityIn);
	}

	protected void resetTask(ServerWorld worldIn, FarlanderEntity entityIn, long gameTimeIn)
	{
		Brain<?> brain = entityIn.getBrain();
		brain.removeMemory(MemoryModuleType.WALK_TARGET);
		brain.removeMemory(MemoryModuleType.LOOK_TARGET);
	}

	protected void updateTask(ServerWorld worldIn, FarlanderEntity owner, long gameTime)
	{
		this.walkAndLookCustomer(owner);
	}

	protected boolean isTimedOut(long gameTime)
	{
		return false;
	}

	private void walkAndLookCustomer(FarlanderEntity owner)
	{
		Brain<?> brain = owner.getBrain();
		brain.setMemory(MemoryModuleType.WALK_TARGET, new WalkTarget(new EntityPosWrapper(owner.getCustomer(), false), this.speed, 2));
		brain.setMemory(MemoryModuleType.LOOK_TARGET, new EntityPosWrapper(owner.getCustomer(), true));
	}
}