package com.legacy.farlanders.entity.ai;

import java.util.List;

import com.legacy.farlanders.entity.FarlanderEntity;

import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.monster.MonsterEntity;

public class GolemStayNearFarlanderGoal extends Goal
{

	private final MonsterEntity enderGolem;

	private FarlanderEntity farlander;

	private final double moveSpeed;

	private int delayCounter;

	public GolemStayNearFarlanderGoal(MonsterEntity golem, double speed)
	{
		this.enderGolem = golem;
		this.moveSpeed = speed;
	}

	public boolean shouldExecute()
	{
		List<FarlanderEntity> list = this.enderGolem.world.getEntitiesWithinAABB(FarlanderEntity.class, this.enderGolem.getBoundingBox().grow(30.0D, 10.0D, 30.0D));
		FarlanderEntity farlanderEntity = null;
		double d0 = Double.MAX_VALUE;

		for (FarlanderEntity nearestFarlanderEntity : list)
		{
			double d1 = this.enderGolem.getDistance(nearestFarlanderEntity);
			if (!(d1 > d0))
			{
				d0 = d1;
				farlanderEntity = nearestFarlanderEntity;
			}
		}
		if (farlanderEntity == null)
		{
			return false;
		}
		else if (d0 < 20.0D)
		{
			return false;
		}
		else
		{
			this.farlander = farlanderEntity;
			return true;
		}
	}

	public boolean shouldContinueExecuting()
	{
		if (!this.farlander.isAlive())
		{
			return false;
		}
		else
		{
			double d0 = this.enderGolem.getDistance(this.farlander);
			return !(d0 < 20.0D) && !(d0 > 256.0D);
		}
	}

	public void startExecuting()
	{
		this.delayCounter = 0;
	}

	public void resetTask()
	{
		this.farlander = null;
	}

	public void tick()
	{
		if (--this.delayCounter <= 0)
		{
			this.delayCounter = 10;
			this.enderGolem.getNavigator().tryMoveToEntityLiving(this.farlander, this.moveSpeed);
		}
	}
}