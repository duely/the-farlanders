package com.legacy.farlanders.entity;

import java.util.EnumSet;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.legacy.farlanders.entity.util.FarlanderTrades;
import com.legacy.farlanders.registry.FarlandersItems;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.LookAtCustomerGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookAtWithoutMovingGoal;
import net.minecraft.entity.ai.goal.MoveTowardsRestrictionGoal;
import net.minecraft.entity.ai.goal.PanicGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.TradeWithPlayerGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.entity.merchant.villager.AbstractVillagerEntity;
import net.minecraft.entity.merchant.villager.VillagerTrades;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MerchantOffer;
import net.minecraft.item.MerchantOffers;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class WandererEntity extends AbstractVillagerEntity
{
	public Int2ObjectMap<VillagerTrades.ITrade[]> wandererTrades = new Int2ObjectOpenHashMap<>();
	private BlockPos targetWanderingPos;
	private int despawnDelay;

	public WandererEntity(EntityType<? extends WandererEntity> typeIn, World worldIn)
	{
		super(typeIn, worldIn);
		this.forceSpawn = true;
		this.setPathPriority(PathNodeType.WATER, -1.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(1, new TradeWithPlayerGoal(this));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, LooterEntity.class, 8.0F, 0.5D, 0.5D));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, RebelEntity.class, 8.0F, 0.5D, 0.5D));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, EnderGolemEntity.class, 8.0F, 0.5D, 0.5D));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, EnderGuardianEntity.class, 20.0F, 0.5D, 0.7D));
		this.goalSelector.addGoal(2, new PanicGoal(this, 0.5D));
		this.goalSelector.addGoal(1, new LookAtCustomerGoal(this));
		this.goalSelector.addGoal(2, new WandererEntity.MoveToGoal(this, 2.0D, 0.35D));
		this.goalSelector.addGoal(4, new MoveTowardsRestrictionGoal(this, 1.0D));
		this.goalSelector.addGoal(8, new WaterAvoidingRandomWalkingGoal(this, 0.35D));
		this.goalSelector.addGoal(9, new LookAtWithoutMovingGoal(this, PlayerEntity.class, 3.0F, 1.0F));
		this.goalSelector.addGoal(10, new LookAtGoal(this, MobEntity.class, 8.0F));
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MobEntity.func_233666_p_().createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.5D);
	}

	@Override
	public AgeableEntity func_241840_a(ServerWorld worldIn, AgeableEntity ageableIn)
	{
		return null;
	}

	@Override
	public boolean hasXPBar()
	{
		return false;
	}

	@Override
	public ActionResultType func_230254_b_(PlayerEntity p_230254_1_, Hand p_230254_2_)
	{
		ItemStack itemstack = p_230254_1_.getHeldItem(p_230254_2_);
		if (itemstack.getItem() != FarlandersItems.wanderer_spawn_egg && this.isAlive() && !this.hasCustomer() && !this.isChild())
		{
			if (p_230254_2_ == Hand.MAIN_HAND)
			{
				p_230254_1_.addStat(Stats.TALKED_TO_VILLAGER);
			}

			if (this.getOffers().isEmpty())
			{
				return ActionResultType.func_233537_a_(this.world.isRemote);
			}
			else
			{
				if (!this.world.isRemote)
				{
					this.setCustomer(p_230254_1_);
					this.openMerchantContainer(p_230254_1_, this.getDisplayName(), 1);
				}

				return ActionResultType.func_233537_a_(this.world.isRemote);
			}
		}
		else
		{
			return super.func_230254_b_(p_230254_1_, p_230254_2_);
		}
	}

	@Override
	protected void populateTradeData()
	{
		VillagerTrades.ITrade[] wandererTradeList = FarlanderTrades.wandererTrades.get(1);

		if (wandererTradeList != null)
		{
			MerchantOffers merchantoffers = this.getOffers();
			this.addTrades(merchantoffers, wandererTradeList, 5);
		}
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("DespawnDelay", this.despawnDelay);
		if (this.targetWanderingPos != null)
		{
			compound.put("WanderTarget", NBTUtil.writeBlockPos(this.targetWanderingPos));
		}
	}

	@Override
	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
		if (compound.contains("DespawnDelay", 99))
		{
			this.despawnDelay = compound.getInt("DespawnDelay");
		}
		if (compound.contains("WanderTarget"))
		{
			this.targetWanderingPos = NBTUtil.readBlockPos(compound.getCompound("WanderTarget"));
		}
		this.setGrowingAge(Math.max(0, this.getGrowingAge()));
	}

	@Override
	public boolean canDespawn(double distanceToClosestPlayer)
	{
		return this.ticksExisted > 20 * 180;
	}

	@Override
	protected void onVillagerTrade(MerchantOffer offer)
	{
		if (offer.getDoesRewardExp())
		{
			int i = 3 + this.rand.nextInt(4);
			this.world.addEntity(new ExperienceOrbEntity(this.world, this.getPosX(), this.getPosY() + 0.5D, this.getPosZ(), i));
		}
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_FARLANDER_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FarlandersSounds.ENTITY_FARLANDER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_FARLANDER_DEATH;
	}

	@Override
	public SoundEvent getYesSound()
	{
		this.playSound(FarlandersSounds.ENTITY_FARLANDER_IDLE, this.getSoundVolume(), 1.3F);
		return null;
	}

	@Override
	protected SoundEvent getVillagerYesNoSound(boolean positive)
	{
		if (positive)
			this.playSound(FarlandersSounds.ENTITY_FARLANDER_IDLE, this.getSoundVolume(), 1.3F);
		else
			this.playSound(FarlandersSounds.ENTITY_FARLANDER_IDLE, this.getSoundVolume(), 0.7F);

		return null;
	}

	public void func_213728_s(int p_213728_1_)
	{
		this.despawnDelay = p_213728_1_;
	}

	public int func_213735_eg()
	{
		return this.despawnDelay;
	}

	@Override
	public void livingTick()
	{
		super.livingTick();

		if (this.isInWaterOrBubbleColumn() == true)
		{
			this.attackEntityFrom(DamageSource.DROWN, 1);
		}

		if (!this.world.isRemote)
		{
			this.func_222821_eh();
		}
	}

	private void func_222821_eh()
	{
		if (this.despawnDelay > 0 && !this.hasCustomer() && --this.despawnDelay == 0)
		{
			this.remove();
		}
	}

	public void setWanderPos(BlockPos pos)
	{
		this.targetWanderingPos = pos;
	}

	@Nullable
	private BlockPos wanderPos()
	{
		return this.targetWanderingPos;
	}

	protected class MoveToGoal extends Goal
	{
		final WandererEntity entity;
		final double maxDistance;
		final double speed;

		protected MoveToGoal(WandererEntity entityIn, double maxDistanceIn, double speedIn)
		{
			this.entity = entityIn;
			this.maxDistance = maxDistanceIn;
			this.speed = speedIn;
			this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE));
		}

		@Override
		public void resetTask()
		{
			this.entity.setWanderPos((BlockPos) null);
			WandererEntity.this.navigator.clearPath();
		}

		@Override
		public boolean shouldExecute()
		{
			BlockPos blockpos = this.entity.wanderPos();
			return blockpos != null && this.canMoveToPos(blockpos, this.maxDistance);
		}

		@Override
		public void tick()
		{
			BlockPos blockpos = this.entity.wanderPos();
			if (blockpos != null && WandererEntity.this.navigator.noPath())
			{
				if (this.canMoveToPos(blockpos, 10.0D))
				{
					Vector3d vec3d = (new Vector3d((double) blockpos.getX() - this.entity.getPosX(), (double) blockpos.getY() - this.entity.getPosY(), (double) blockpos.getZ() - this.entity.getPosZ())).normalize();
					Vector3d vec3d1 = vec3d.scale(10.0D).add(this.entity.getPosX(), this.entity.getPosY(), this.entity.getPosZ());
					WandererEntity.this.navigator.tryMoveToXYZ(vec3d1.x, vec3d1.y, vec3d1.z, this.speed);
				}
				else
				{
					WandererEntity.this.navigator.tryMoveToXYZ((double) blockpos.getX(), (double) blockpos.getY(), (double) blockpos.getZ(), this.speed);
				}
			}
		}

		private boolean canMoveToPos(BlockPos posIn, double distanceIn)
		{
			return !posIn.withinDistance(this.entity.getPositionVec(), distanceIn);
		}
	}
}