package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemMysticWandRegeneration extends MysticWandBaseItem
{
	public ItemMysticWandRegeneration(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(TextFormatting.RED);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
	{
		ItemStack itemstack = playerIn.getHeldItem(handIn);

		if (!worldIn.isRemote)
		{
			if (playerIn.isPotionActive(Effects.REGENERATION))
			{
				TranslationTextComponent chat = new TranslationTextComponent("gui.item.wand.regeneration.failure");
				playerIn.sendStatusMessage(chat, true);
				return new ActionResult<>(ActionResultType.FAIL, itemstack);
			}
			else
				playerIn.addPotionEffect(new EffectInstance(Effects.REGENERATION, 20 * 20, 0));

			itemstack.damageItem(1, playerIn, (player) -> player.sendBreakAnimation(playerIn.getActiveHand()));
		}

		if (!playerIn.isPotionActive(Effects.REGENERATION))
		{
			playerIn.getCooldownTracker().setCooldown(this, 20 * 20);
			worldIn.playSound(playerIn, playerIn.getPosition(), SoundEvents.ENTITY_ZOMBIE_VILLAGER_CURE, SoundCategory.PLAYERS, 1.0F, 1.0F);
		}

		playerIn.addStat(Stats.ITEM_USED.get(this));
		return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
	}

	@Override
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add(new TranslationTextComponent("gui.item.wand.regeneration").mergeStyle(TextFormatting.GRAY));
	}
}