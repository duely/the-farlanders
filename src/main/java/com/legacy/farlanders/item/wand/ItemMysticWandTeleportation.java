package com.legacy.farlanders.item.wand;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.gen.Heightmap;

public class ItemMysticWandTeleportation extends MysticWandBaseItem
{

	public ItemMysticWandTeleportation(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(TextFormatting.LIGHT_PURPLE);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
	{
		ItemStack itemstack = playerIn.getHeldItem(handIn);

		Random rand = worldIn.getRandom();

		int randX = 0;
		int randY = 0;
		int randZ = 0;
		if (!worldIn.isRemote && playerIn instanceof ServerPlayerEntity)
		{
			ServerPlayerEntity serverPlayer = (ServerPlayerEntity) playerIn;
			BlockPos spawnpoint = serverPlayer.func_241140_K_();
			boolean bedWorks = worldIn.getDimensionType().doesBedWork();
			boolean anchorWorks = worldIn.getDimensionType().doesRespawnAnchorWorks();
			boolean canSpawnHere = bedWorks || anchorWorks;

			if (!bedWorks && !anchorWorks || spawnpoint == null || spawnpoint != null && spawnpoint == BlockPos.ZERO)
			{
				TranslationTextComponent chat = new TranslationTextComponent("gui.item.wand.teleportation.failure");
				playerIn.sendStatusMessage(chat, true);
				return new ActionResult<>(ActionResultType.FAIL, itemstack);
			}
			else if (canSpawnHere)
			{
				try
				{
					int worldHeight = worldIn.getHeight(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, spawnpoint.getX(), spawnpoint.getZ());
					int height = worldHeight < worldIn.getDimensionType().getLogicalHeight() ? worldHeight : spawnpoint.getY() + 1;
					playerIn.setPositionAndUpdate(spawnpoint.getX() + 0.5F, height, spawnpoint.getZ() + 0.5F);

					if (!canSpawnHere)
					{
						while (!canSpawnHere)
						{
							randX = rand.nextInt(20);
							randY = rand.nextInt(20);
							randZ = rand.nextInt(20);

							height = worldIn.getHeight(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, spawnpoint.getX() + randX, spawnpoint.getZ() + randZ);
							playerIn.setPositionAndUpdate(spawnpoint.getX() + randX, height, spawnpoint.getZ() + randZ);
						}
					}
				}
				catch (Exception e)
				{
					playerIn.setPositionAndUpdate(spawnpoint.getX(), spawnpoint.getY() + 5, spawnpoint.getZ());
					if (!canSpawnHere)
					{
						while (!canSpawnHere)
						{
							randX = rand.nextInt(20);
							randY = rand.nextInt(20);
							randZ = rand.nextInt(20);
							playerIn.setPositionAndUpdate(spawnpoint.getX() + randX, spawnpoint.getY() + randY, spawnpoint.getZ() + randZ);
						}
					}
				}
				worldIn.playSound(playerIn, spawnpoint, SoundEvents.BLOCK_PORTAL_TRAVEL, SoundCategory.PLAYERS, 1.0F, 1.0F);
				itemstack.damageItem(1, playerIn, (p_220009_1_) ->
				{
					p_220009_1_.sendBreakAnimation(playerIn.getActiveHand());
				});
			}
		}

		worldIn.playSound(playerIn, playerIn.getPosition(), SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1.0F, 1.0F);
		playerIn.addStat(Stats.ITEM_USED.get(this));
		return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
	}

	@Override
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add(new TranslationTextComponent("gui.item.wand.teleportation").mergeStyle(TextFormatting.GRAY));
	}
}