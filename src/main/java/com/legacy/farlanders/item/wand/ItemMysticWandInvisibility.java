package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.client.audio.FarlandersSounds;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemMysticWandInvisibility extends MysticWandBaseItem
{
	public ItemMysticWandInvisibility(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(TextFormatting.AQUA);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
	{
		ItemStack itemstack = playerIn.getHeldItem(handIn);

		if (!worldIn.isRemote)
		{
			if (playerIn.isPotionActive(Effects.INVISIBILITY))
			{
				TranslationTextComponent chat = new TranslationTextComponent(I18n.format("gui.item.wand.invisibility.failure"));
				playerIn.sendStatusMessage(chat, true);
			}
			else
			{
				playerIn.addPotionEffect(new EffectInstance(Effects.INVISIBILITY, 30 * 20, 0));
			}

			itemstack.damageItem(1, playerIn, (p_220009_1_) ->
			{
				p_220009_1_.sendBreakAnimation(playerIn.getActiveHand());
			});
		}

		if (!playerIn.isPotionActive(Effects.INVISIBILITY))
		{
			playerIn.getCooldownTracker().setCooldown(this, 30 * 20);
			worldIn.playSound(playerIn, playerIn.getPosition(), FarlandersSounds.ENTITY_MYSTIC_ENDERMAN_BLINDNESS, SoundCategory.PLAYERS, 1.0F, 1.0F);
		}

		playerIn.addStat(Stats.ITEM_USED.get(this));
		return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
	}

	@Override
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add(new TranslationTextComponent("gui.item.wand.invisibility").mergeStyle(TextFormatting.GRAY));
	}
}